Fonctionnalité: Modification de mon profil utilisateur
  Afin de garder à jour mes informations personnelles
  En tant qu'utilisateur connecté
  Je peux les modifier dans mon profil

  Scénario: Changement de mot de passe
    Étant donné que je me connecte en tant que "nurse:nurse"
    Et que j'accède à l'administration de mon profil
    Lorsque je souhaite mettre "Bépoè!42" comme mot de passe
    Alors une notification visuelle m'informe que tout s'est bien passé
