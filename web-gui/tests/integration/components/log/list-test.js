import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | log/list', function(hooks) {
  setupRenderingTest(hooks);

  skip('it renders logs as an ordered list', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let lastNameLogAction = store.createRecord('log-action', { name: "updated", changes: { lastName: ["Dupond","Dupont"]} })
    let birthYearLogAction = store.createRecord('log-action', { name: "updated", changes: { birthYear: [2000,2001]} })
    let lastNameLog = store.createRecord('log', { actions: [lastNameLogAction], occuredAt: "2021-12-13T09:24Z", who: "Greg House" })
    let birthYearLog = store.createRecord('log', { actions: [birthYearLogAction], occuredAt: "2021-12-13T10:42", who: "Greg House" })
    // Following sentence fails with:
    // The 'patient' type does not implement 'loggable' and thus cannot be assigned to the 'loggable' relationship in 'log'. Make it a descendant of 'loggable' or use a mixin of the same name
    let patient = store.createRecord('patient', { logs: [lastNameLog, birthYearLog] });
    this.set('loggable', patient);

    // When
    await render(hbs`<Log::List @loggable={{patient}} />`);

    // Then
    assert.ok( true, this.element.querySelector('div[data-test-logs-list] > ol > li:nth-of-type(1)').textContent.trim().match('Modifié'));
    // many other tests to come...
  });
});
