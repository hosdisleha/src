/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | patient/edit-form', function(hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders common inputs', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let patient = store.createRecord('patient', {lastName: 'Rella', firstName: 'Cindy', gender: 'woman'});
    this.set('patient', patient);

    // When
    await render(hbs`<Patient::EditForm @patient={{patient}} />`);

    // Then
    assert.dom(this.element.querySelector('span[data-test-patientgender]')).hasText('t:patient.gender.woman:()');
    assert.equal(this.element.querySelector('div[data-test-lastname] input').value, 'Rella');
    assert.equal(this.element.querySelector('div[data-test-firstname] input').value, 'Cindy');
    assert.dom('div[data-test-birthdate]').exists()
    assert.dom('div[data-test-birthyear]').exists()
  });

  test('it renders only birthdate input if present', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let patient = store.createRecord('patient', {birthdate: '09/09/2000'});
    this.set('patient', patient);

    // When
    await render(hbs`<Patient::EditForm @patient={{patient}} />`);

    // Then
    assert.equal(this.element.querySelector('div[data-test-birthdate] input').value, '09/09/2000');
    assert.dom('div[data-test-birthyear]').doesNotExist()
  });

  test('it renders both birthdate and birthyear inputs if only birthyear present', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let patient = store.createRecord('patient', {birthYear: '2000'});
    this.set('patient', patient);

    // When
    await render(hbs`<Patient::EditForm @patient={{patient}} />`);

    // Then
    assert.equal(this.element.querySelector('div[data-test-birthyear] input').value, '2000');
    assert.dom('div[data-test-birthyear]').exists()
  });
});
