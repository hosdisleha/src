/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Unit | Controller | patients/index/duplicating', function(hooks) {
  setupTest(hooks);
  setupMirage(hooks);

  test('save moves probablyDuplicatedPatient to duplicatedPatient', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let duplicatedPatient = store.createRecord('patient', {id: 1});
    let patient = store.createRecord('patient', {id: 2, probablyDuplicatedPatient: duplicatedPatient});
    let controller = this.owner.lookup('controller:patients/index/duplicating');
    controller.set('model', patient);
    // When
    controller.send('save')
    // Then
    assert.strictEqual(controller.get('patient.duplicatedPatient.id'), "1");
    assert.strictEqual(controller.get('patient.probablyDuplicatedPatient.id'), undefined);
  });
});
