/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';

module('Unit | Model | log action', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks);

  test('intlName returns localized action\'s name', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('log-action', {name: 'cancelled'});
    //When
    let intlName = model.get('intlName')
    //Then
    assert.equal(intlName, 't:log-action.name.cancelled:()');
  });

  test('intlSource returns localized source', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('log-action', {source: 'umj'});
    //When
    let intlSource = model.get('intlSource')
    //Then
    assert.equal(intlSource, 't:appointment.cancelledBy.umj:()');
  });

  test('intlChanges returns localized changes (someday)', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('log-action', {changes: { last_name: ["Dupond","Dupont"], birth_year: [2000,2001]}});
    //When
    let intlChanges = model.get('intlChanges')
    //Then
    assert.equal(intlChanges, 'last_name : Dupond => Dupont // birth_year : 2000 => 2001');
  });

  test('intlLabel returns intlName if NO source provided', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('log-action', {name: 'updated'});
    //When
    let intlLabel = model.get('intlLabel')
    //Then
    assert.equal(intlLabel, model.get('intlName'));
  });

  test('intlLabel returns intlName and intlSource if source provided', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('log-action', {name: 'cancelled', source: 'patient'});
    //When
    let intlLabel = model.get('intlLabel')
    //Then
    assert.ok(true, intlLabel.match(model.get('intlName')));
    assert.ok(true, intlLabel.match(model.get('intlSource')));
  });
});
