/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';

module('Unit | Model | patient', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks);

  test('#abbreviatedGender', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('patient', {gender: 'woman'});
    // When
    let result = model.abbreviatedGender;
    // Then
    assert.equal(result, 't:patient.gender.abbreviated.woman:()');
  });

  test('#fullName uppercases last name and capitalize first name', function(assert) {
    let store = this.owner.lookup('service:store');
    // Given
    let model = store.createRecord('patient', {lastName: 'lastname', firstName: 'firstname'});
    // When
    let result = model.fullName;
    // Then
    assert.equal(result, 'LASTNAME Firstname');
  });

  test('#fullName returns only the last name when no first name provided', function(assert) {
    let store = this.owner.lookup('service:store');
    // Given
    let model = store.createRecord('patient', {lastName: 'lastname'});
    // When
    let result = model.fullName;
    // Then
    assert.equal(result, 'LASTNAME');
  });

  test('#fullName returns only the first name when no last name provided', function(assert) {
    let store = this.owner.lookup('service:store');
    // Given
    let model = store.createRecord('patient', {firstName: 'firstname'});
    // When
    let result = model.fullName;
    // Then
    assert.equal(result, 'Firstname');
  });

  test('#fullName returns a lowercased first name', function(assert) {
    let store = this.owner.lookup('service:store');
    // Given
    let model = store.createRecord('patient', {firstName: 'FIRSTNAME'});
    // When
    let result = model.fullName;
    // Then
    assert.equal(result, 'Firstname');
  });

  test('#fullName returns nothing when nothing provided', function(assert) {
    let store = this.owner.lookup('service:store');
    // Given
    let model = store.createRecord('patient');
    // When
    let result = model.fullName;
    // Then
    assert.equal(result, '');
  });

  test('#humanGender', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('patient', {gender: 'woman'});
    // When
    let result = model.humanGender;
    // Then
    assert.equal(result, 't:patient.gender.woman:()');
  });

  test('#humanHistoryCount', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('patient');
    // When
    let result = model.humanHistoryCount;
    // Then
    assert.equal(result, 't:patient.history.count:("nb":0)');
  });
});
