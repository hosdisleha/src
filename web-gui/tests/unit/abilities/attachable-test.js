/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import 'ember-qunit';
import { run } from '@ember/runloop'
import helpers from './helpers'

module('Unit | Ability | attachable', function(hooks) {
  setupTest(hooks)

  let subject = function (attachable) {
    let ability = this.owner.lookup('ability:attachable');
    ability.set('model', attachable);

    return ability;
  }

  // Appointments

  let createAppointment = function (...roles) {
    let store = this.owner.lookup('service:store')
    let consultationType = run(() => { return store.createRecord('consultation-type')})
    consultationType.get('assignedRoles').pushObjects(roles)
    let appointment = run(() => { return store.createRecord('appointment', { consultationType: consultationType})});

    return appointment;
  }

  test('admin can attach any file to an appointment', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'administrator');
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('nurse can attach any file to an appointment', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse');
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('secretary can attach any file to an appointment', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary');
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('user with medical confidentiality role can attach any file if she can take an appointment in charge', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'any', true);
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('user with medical confidentiality cannot attach any file if she cannot take an appointment in charge', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.notOk(ability.get('canAttachFiles'))
  })

  test('user without medical confidentiality role cannot attach any file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', false);
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.notOk(ability.get('canAttachFiles'))
  })

  test('user with medical confidentiality role can access to medical content', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'physician', true);
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canConsultMedicalContent'))
  })

  test('user without medical confidentiality role cannot access to medical content', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'association', false);
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.notOk(ability.get('canConsultMedicalContent'))
  })

  // Flowing consultations

  let createFlowingConsultation = function () {
    let store = this.owner.lookup('service:store')
    let interventionDomain = run(() => { return store.createRecord('intervention-domain')})
    let flowingConsultation = run(() => { return store.createRecord('flowing-consultation', { interventionDomain: interventionDomain})});

    return flowingConsultation;
  }

  test('admin can attach any file to a flowing consultation', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'administrator');
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('nurse can attach any file to a flowing consultation', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse');
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('secretary can attach any file to a flowing consultation', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary');
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('physician can attach any file to flowing consultations', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'physician', true);
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.ok(ability.get('canAttachFiles'))
  })

  test('unpriviledged user cannot attach any file to flowing consultations', function(assert) {
    helpers.setCurrentUserForRole.call(this);
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.notOk(ability.get('canAttachFiles'))
  })

  test('user with medical confidentiality role can access to medical content', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'physician', true);
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.ok(ability.get('canConsultMedicalContent'))
  })

  test('user without medical confidentiality role cannot access to medical content', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'association', false);
    let flowingConsultation = createFlowingConsultation.call(this)

    let ability = subject.call(this, flowingConsultation);

    assert.notOk(ability.get('canConsultMedicalContent'))
  })

});
