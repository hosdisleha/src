/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { run } from '@ember/runloop';
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupPact, given, interaction } from 'ember-cli-pact';
import { authenticateSession } from 'ember-simple-auth/test-support';

module('Pact | web-api | Certificate layout', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'web-api_certificate-layout'
  });

  hooks.beforeEach(async function() {
    given('the certificate layout exists');
    await authenticateSession({
      access_token: 'my_secret_token',
      token_type: 'bearer',
      expires_in: 7200
    });
  });

  test('fetching through settings', async function(assert) {
    // When
    let result = await interaction(() => this.store().findRecord('certificate-layout', "certificate_layout"));
    // Then
    assert.equal(result.get('id'), 'certificate_layout');
  });

  test('updating through settings', async function(assert) {
    // Given
    let newOrganisationChart = 'Lorem ipsum';
    let certificateLayout = await run(() => {
      let promisedLayout = this.store().findRecord('certificate-layout', "certificate_layout");

      promisedLayout.then((layout) => {
        layout.set('organisationChart', newOrganisationChart);
      })

      return promisedLayout;
    })
    // When
    let result = await interaction(() => certificateLayout.save());
    // Then
    assert.equal(result.get('organisationChart'), newOrganisationChart);
  });
});
