/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'web-gui',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false,
      },
    },
    flashMessageDefaults: {
      // flash message defaults
      timeout: 5000,
      extendedTimeout: 700,
      // priority: 200,
      // sticky: true,
      // showProgress: true,

      // service defaults
      // type: 'info',
      types: [ 'info', 'success', 'warning', 'danger' ],
      preventDuplicates: true
    },

    emberFullCalendar: {
      includeScheduler: true,
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      includeLocales: ['fr']
    },

    'ember-cli-uuid': {
      defaultUUID: false
    },

    "ember-cli-pact": {
      pactsDirectory: 'pacts/generated',
      pactVersion: 2
    },

    moment: {
      // Options:
      // 'all' - all years, all timezones
      // 'subset' - 10-year range, all timezones
      // 'none' - no data, just timezone API
      includeTimezone: 'subset'
    },

    featureFlags: {
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
      API_URL: process.env.API_URL,
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV['ember-cli-mirage'] = {
      enabled: false
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }


  return ENV;
};
