/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import { task, timeout } from 'ember-concurrency';
import moment from 'moment';

const refreshInterval = 60*1000; // 60s

export default Route.extend(AuthenticatedRouteMixin, {
  can: service(),
  flashMessages: service(),

  model(params) {
    return hash({
      interventionDomain: this.store.findRecord('intervention-domain', params.intervention_domain_id)
    });
  },

  afterModel(model) {
    let interventionDomain = model.interventionDomain
    if (this.can.cannot('consult flowing consultations of intervention-domain', interventionDomain)) {
      this.flashMessages.danger("Vous n'êtes pas habilité·e·s à accéder à cette information.")
      return this.transitionTo('index');
    }

    return model.flowingConsultations = this.store.query('flowing-consultation', {
      filter: {
        intervention_domain_id: interventionDomain.get('id'),
        state: 'active'
      }
    });
  },

  setupController(controller, model) {
    this._super(...arguments);
    controller.set('lastUpdateCheck', moment().toDate());
    this.pollServerForChanges.perform(model.interventionDomain);
  },

  pollServerForChanges: task(function * (interventionDomain) {
    let needToRefresh = false;
    let message = 'Une ou plusieurs consultations ont été mises à jour. Cliquer ici pour recharger la page.';

    while(true) {
      yield timeout(refreshInterval);
      if (needToRefresh == false) {
        this.store.queryRecord('flowing-consultation', {
          last_updated: true,
          filter: {
            'intervention_domain_id': interventionDomain.id
          }
        }).then( (flowingConsultation) => {
          let lastUpdatedAt = flowingConsultation.get('updatedAt')

          if (lastUpdatedAt > this.controller.get('lastUpdateCheck')) {
            needToRefresh = true;
            let currentRoute = this;

            this.flashMessages.warning(message, {
              sticky: true,
              onDestroy: function () { currentRoute.refresh() }
            })
          }
        }).catch( (err) => {
          this.flashMessages.danger(err.message);
        })
      }
    }
  }).cancelOn('deactivate').restartable()
});
