/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';
import { computed, or, raw, getBy } from 'ember-awesome-macros';
import { equal } from '@ember/object/computed';

export default Component.extend(FileSaverMixin, {
  store: service(),
  intl: service(),

  readOnly: false,
  type: null,

  concernsValidatedCerticate: equal('type', 'validated-certificate'),

  relationForType: Object.freeze({
    'standard': 'standardAttachedFiles',
    'medical-content': 'medicalContentFiles',
    'redacted-certificate': 'redactedCertificates',
    'signed-certificate': 'signedCertificates',
    'validated-certificate': 'validatedCertificates',
    null: 'attachedFiles'
  }),
  relation: getBy('relationForType', 'type'),
  attachedFiles: getBy('attachable', 'relation'),

  typeWithDefault: or('type', raw('standard')),
  nameLabel: computed('typeWithDefault', function(typeWithDefault) {
    let key = 'attached-file.' + typeWithDefault + '.singular';

    return this.intl.t(key);
  }),
  title: computed('typeWithDefault', 'attachedFiles.length', function(typeWithDefault, length) {
    let key = 'attached-file.' + typeWithDefault + '.count';

    return this.intl.t(key, { nb: length });
  }),
  hint: computed('typeWithDefault', function(typeWithDefault) {
    let key = 'attached-file.' + typeWithDefault + '.hint';

    return this.intl.t(key);
  }),

  actions: {
    deleteAttachedFile(attachedFile) {
      attachedFile.destroyRecord().then( () => {
        this.toggleProperty('shouldReload')
      }).catch(() => {
        this.flashMessages.danger('Impossible de supprimer le fichier attaché.')
      })
    },

    downloadAttachedFile(attachedFile) {
      attachedFile.download().then((content) => {
        this.saveFileAs(attachedFile.get('filename'), content, attachedFile.get('contentType'))
      }).catch(() => {
        this.flashMessages.danger('Impossible de récupérer le fichier.')
      });
    },

    uploadAttachedFile(file) {
      let attachable =  this.attachable;
      let type = this.typeWithDefault;
      let adapter = this.store.adapterFor('application')
      let baseURL = adapter.buildURL(attachable.constructor.modelName, attachable.get('id'))
      let authHeader = adapter.get('authHeader')

      file.upload(`${baseURL}/attached_files?type=${type}`, { headers: authHeader}).then( response => {
        if(response.status < 400) {
          this.store.pushPayload(response.body)
        } else {
          this.flashMessages.danger('Impossible d\'attacher ce fichier.')
        }
      })
    }
  }
});
