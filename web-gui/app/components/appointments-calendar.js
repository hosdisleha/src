/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class AppointmentsCalendar extends Component {
  @service store;
  @service router;

  @action
  showEvent(event) {
    this.router.transitionTo(
      this.router.currentRoute.parent.name + '.show',
      event.appointmentId
    );
  }

  @action
  showForm(consultationType, start /*, end*/) {
    let targetRoute = [];

    let currentRouteName = this.router.currentRouteName;

    if (currentRouteName === 'consultation-type.appointments.edit') {
      targetRoute.pushObject(currentRouteName);
    }
    else {
      targetRoute.pushObject('consultation-type.appointments.new');
    }

    if (consultationType) {
      targetRoute.pushObject(consultationType);
    }

    targetRoute.pushObject({
      queryParams: {
        startsAt: start.toISOString(),
      },
    });

    this.router.transitionTo(...targetRoute);
  }
}
