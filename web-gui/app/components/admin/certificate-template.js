/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';

export default class AdminCertificateTemplateComponent extends Component {
  @service store
  @service flashMessages

  @action
  disable() {
    let certificateTemplate = this.args.certificateTemplate
    let templateTitle = certificateTemplate.get('title')
    certificateTemplate.set('disabledAt', moment().toDate());

    certificateTemplate.save().then(() => {
      this.flashMessages.success(`Le modèle de certificate ${templateTitle} vient d'être désactivé.`);
    }).catch(() => {
      this.flashMessages.danger(`Impossible de désactiver le modèle de certificat ${templateTitle}.`)
    });
  }

  @action
  enable() {
    let certificateTemplate = this.args.certificateTemplate
    let templateTitle = certificateTemplate.get('title')
    certificateTemplate.set('disabledAt', null);

    certificateTemplate.save().then(() => {
      this.flashMessages.success(`Le modèle de certificate ${templateTitle} vient d'être activé.`);
    }).catch(() => {
      this.flashMessages.danger(`Impossible d'activer le modèle de certificat ${templateTitle}.`)
    });
  }

  @action
  uploadMasterDocument(file) {
    let certificateTemplate = this.args.certificateTemplate
    let attachable = certificateTemplate
    let adapter = this.store.adapterFor('application')
    let baseURL = adapter.buildURL(attachable.constructor.modelName, attachable.get('id'))
    let authHeader = adapter.get('authHeader')

    file.upload(`${baseURL}/master_document`, { headers: authHeader}).then( response => {
      if(response.status < 400) {
        this.store.pushPayload(response.body)
      } else {
        this.flashMessages.danger('Impossible d\'attacher ce fichier.')
      }
    })
  }

  @action
  deleteMasterDocument() {
    let certificateTemplate = this.args.certificateTemplate
    certificateTemplate.get('masterDocument').then((masterDocument) => {
      masterDocument.destroyRecord().then(() => {
        this.flashMessages.success('Modèle de document supprimé avec succès.')
      }).catch(() => {
        this.flashMessages.danger('Impossible de supprimer ce modèle de document.')
      })
    })
  }

  @action
  saveQuestionnaireJson(json) {
    let certificateTemplate = this.args.certificateTemplate
    let templateTitle = certificateTemplate.get('title')
    certificateTemplate.set('questionnaire', json);

    certificateTemplate.save().then(() => {
      this.flashMessages.success(`Le questionnaire du modèle de certificat ${templateTitle} vient d'être mis à jour.`);
    }).catch(() => {
      certificateTemplate.rollbackAttributes();
      this.flashMessages.danger(`Impossible d'enregistrer le questionnaire du modèle de certificat ${templateTitle}.`)
    });
  }
}
