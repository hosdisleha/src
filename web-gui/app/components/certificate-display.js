/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import Survey from 'survey-jquery';
import { assign } from '@ember/polyfills';
import { reads } from '@ember/object/computed';
import moment from 'moment';
import jQuery from 'jquery'

export default Component.extend({
  questionnaire: reads('attachable.certificateQuestionnaire'),
  data: reads('attachable.certificateData'),

  didRender() {
    this._super(...arguments);

    Survey.StylesManager.applyTheme('default');

    let survey = new Survey.Model(this.questionnaire);
    let container = this.element.querySelector('.Certificate--questionnaire');
    let canEdit = this.canEdit
    let data = JSON.parse(this.data || "{}")

    if (!canEdit) { survey.mode = 'display' }
    else {
      data["filae-praticien-prenom"] = this.get('attachable.userInCharge.firstName')
      data["filae-praticien-nom"] = this.get('attachable.userInCharge.lastName')
      data["filae-consultation-date"] = moment(this.get('attachable.startsAt')).format('DD/MM/YYYY')
      data["filae-consultation-heure"] = moment(this.get('attachable.startsAt')).format('HH [heures] mm')
      data["filae-consultation-motif"] = this.get('attachable.consultationReason.title')
      data["filae-service-requerant"] = this.get('attachable.callingInstitution.title')
      data["filae-opj-nom-complet"] = this.get('attachable.officerName')
      data["filae-proces-verbal"] = this.get('attachable.officialReport')
      data["filae-patient-prenom"] = this.get('attachable.patient.firstName')
      data["filae-patient-nom"] = this.get('attachable.patient.lastName')
      data["filae-patient-genre"] = this.get('attachable.patient.humanGender')
      data["filae-patient-date-naissance"] = moment(this.get('attachable.patient.birthdate')).format('DD/MM/YYYY')
    }
    survey.data = assign(survey.data, data);
    survey.locale = 'fr'
    survey.completeText = 'Enregistrer le brouillon'

    jQuery(container).Survey({
      model: survey,
      onComplete: this.onComplete
    })
  }
});
