/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';

export default class PostponeTransition extends Component {
  @service router;

  @action
  postpone(origin) {
    let consultationTypeId = this.args.appointment.consultationType.get('id');

    this.router.transitionTo(
      'consultation-type.appointments.new',
      consultationTypeId,
      {
        queryParams: {
          focusOn: moment(this.args.appointment.startsAt).format('YYYY-MM-DD'),
          copyFromId: this.args.appointment.id,
          postponedBy: origin,
          startsAt: null,
        },
      }
    );
  }
}
