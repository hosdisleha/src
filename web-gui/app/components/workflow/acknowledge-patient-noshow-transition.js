/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';

export default Component.extend({
  actions: {
    patientMissing() {
      let appointment = this.appointment;

      appointment.set('patientMissing', true)
      appointment.save().then(() => {
        this.flashMessages.success("La non-venue du patient est bien enregistrée.")
      }).catch(() => {
        appointment.rollbackAttributes();
        this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer, et si cela se reproduit, de prendre contact avec un administrateur.')
      });
    },

    patientNotMissing() {
      let appointment = this.appointment;

      appointment.set('patientMissing', false);
      appointment.save().then(() => {
        this.flashMessages.success("Finalement, il semblerait que le patient puisse encore arriver. C'est enregistré.")
      }).catch(() => {
        appointment.rollbackAttributes();
        this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer, et si cela se reproduit, de prendre contact avec un administrateur.')
      });
    },
  }
});
