/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import moment from 'moment';

export default Component.extend({
  actions: {
    patientArrived() {
      let appointment = this.appointment;

      appointment.set('patientArrivedAt', moment().toDate())
      appointment.save().then(() => {
        this.flashMessages.success("L'arrivée du patient est enregistrée, il peut se rendre en salle d'attente.")
      }).catch(() => {
        appointment.rollbackAttributes();
        this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer, et si cela se reproduit, de prendre contact avec un administrateur.')
      });
    },

    patientNotArrived() {
      let appointment = this.appointment;

      appointment.set('patientArrivedAt', null);
      appointment.save().then(() => {
        this.flashMessages.success("Finalement, il semblerait que le patient ne soit pas encore arrivé. C'est enregistré.")
      }).catch(() => {
        appointment.rollbackAttributes();
        this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer, et si cela se reproduit, de prendre contact avec un administrateur.')
      });
    },
  }
});
