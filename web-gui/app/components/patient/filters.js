/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';
import { dateFormat, genders } from '../../constants';
import { Changeset } from 'ember-changeset';
import moment from 'moment';

export default class PatientEditFormComponent extends Component {
  dateFormat = dateFormat;

  @tracked
  lastName = null;
  @tracked
  firstName = null;
  @tracked
  birthdate = null;
  @tracked
  birthYear = null;

  constructor() {
    super(...arguments);

    this.lastName = this.args.lastName;
    this.firstName = this.args.firstName;
    this.birthdate = this.args.birthdate;
    this.birthYear = this.args.birthYear;
  }

  @action
  filter() {
    let birthMoment = this.birthdate && moment(this.birthdate);

    this.args.onFilter(this.lastName, this.firstName, birthMoment, this.birthYear)
  }

  @action
  reset() {
    this.lastName = null;
    this.firstName = null;
    this.birthdate = null;
    this.birthYear = null;

    this.filter();
  }
}
