/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Ability } from 'ember-can';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { or, reads } from '@ember/object/computed'

export default Ability.extend({
  currentUser: service(),

  attachable: reads('model'),
  attachableModelName: or('attachable.constructor.modelName', 'attachable.content.constructor.modelName'),

  canAttachFiles: computed('currentUser.{isAdmStaff,medicalConfidentiality}', 'checkAttachable', function() {
    if (this.get('currentUser.isAdmStaff')) { return true }

    return this.get('currentUser.medicalConfidentiality') && this.checkAttachable;
  }),
  checkAttachable: computed('attachableModelName', 'currentUser.isPhysician', 'hasAssignedRole', function() {
    switch (this.attachableModelName) {
      case 'appointment':
      case 'certificate':
        return this.hasAssignedRole;
      case 'flowing-consultation':
        return this.get('currentUser.isPhysician')
      default:
        return false
    }
  }),
  attachableHasAssignedRole: or('attachable.{consultationType,appointment.consultationType}.hasAssignedRole'),
  hasAssignedRole: computed('currentUser.role', 'attachableHasAssignedRole', function() {
    return this.attachableHasAssignedRole(this.get('currentUser.role'));
  }),

  canConsultMedicalContent: reads('currentUser.medicalConfidentiality')
});
