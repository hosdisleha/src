/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  session: service(),

  actions: {
    save(changeset) {
      return changeset.save().then(() => {
        this.flashMessages.success('Création réussie.')
        this.transitionToRoute('flowing-consultations', changeset.get('interventionDomain.id'));
      });
    },

    validate({ key, newValue }) {
      let errors = []

      if ('consultationType' === key) {
        if (!newValue) {
          errors.pushObject('cette information doit être renseignée.')
        }
      }

      return errors.length === 0 || errors
    }
  }
});
