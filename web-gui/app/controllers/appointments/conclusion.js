/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { reads } from '@ember/object/computed';
import moment from 'moment';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';

export default Controller.extend(FileSaverMixin, {
  appointment: reads('model'),

  actions: {
    patientIsReleased(appointment) {
      appointment.set('patientReleasedAt', moment());

      return appointment.save().catch(() => {
        this.flashMessages.danger('Impossible de valider le départ du patient.')
      });
    },

    patientIsNotReleased(appointment) {
      appointment.set('patientReleasedAt', null);

      return appointment.save().catch(() => {
        this.flashMessages.danger('Impossible d\'annuler le départ du patient.')
      });
    },

    backToAppointements() {
      let appointment = this.appointment;

      this.transitionToRoute('consultation-type.appointments', appointment.get('consultationType.id'), {
        queryParams: {
          focusOn: moment(appointment.get('startsAt')).format('YYYY-MM-DD')
        }
      });
    },

    downloadPreview(appointment) {
      appointment.certificatePreview().then((content) => {
        this.saveFileAs(appointment.get('certificatePreviewFilename'), content, 'application/pdf')
      }).catch((e) => {
        this.flashMessages.danger("Impossible de récupérer l'aperçu. " + e.message)
      });
    },

    downloadValidatedCertificate() {
      let attachedFile = this.get('appointment.validatedCertificate');

      attachedFile.download().then((content) => {
        this.saveFileAs(attachedFile.get('filename'), content, attachedFile.get('contentType'))
      }).catch((e) => {
        this.flashMessages.danger("Impossible de récupérer le certificat validé. " + e.message)
      });
    },

    validateCertificate(appointment) {
      return appointment.get('certificate').then((certificate) => {
        certificate.set('validatedAt', moment());

        return certificate.save().then(() => {
          this.flashMessages.success('Certificat validé.');
        }).catch(() => {
          this.flashMessages.danger('Impossible de valider le certificat.')
        });
      })
    }
  }
})
