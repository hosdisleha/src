/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller, { inject as controller } from '@ember/controller';
import QueryParams from 'ember-parachute';
import { computed, observer } from '@ember/object';
import { reads } from '@ember/object/computed';
import { on } from '@ember/object/evented';
import { allSettled, hashSettled } from 'rsvp';
import moment from 'moment';
import CalendarDay from '../../models/calendar-day'
import DS from 'ember-data';

const dateParamFormat = 'YYYY-MM-DD';
const columnMinWidth = 200;

export const myQueryParams = new QueryParams({
  focusOn: {
    defaultValue: moment(),
    refresh: true,
    replace: true,
    serialize(value) {
      return moment(value).format(dateParamFormat);
    },
    deserialize(value) {
      return moment(value, dateParamFormat, true);
    }
  }
});

export default Controller.extend(myQueryParams.Mixin, {
  application: controller(),

  assigning: false,
  displayHidden: false,
  viewportWidth: computed(function() {
    return window.innerWidth
  }),
  nbOfDays: computed('viewportWidth', function() {
    return Math.floor(this.viewportWidth/columnMinWidth);
  }),
  nbBeforeDays: computed('nbOfDays', function() {
    switch(this.nbOfDays) {
      case 0:
      case 1:
      case 2:
        return 0
      case 3:
      case 4:
        return 1
      default:
        return 2
    }
  }),
  nbAfterDays: computed('nbOfDays', function() {
    switch(this.nbOfDays) {
      case 0:
      case 1:
        return 0
      case 2:
      case 3:
        return 1
      default:
        return 2
    }
  }),

  consultationType: reads('model'),
  pivotDate: reads('allQueryParams.focusOn'),

  allDates: computed('pivotDate', 'nbBeforeDays', 'nbAfterDays', function () {
    let pivotDate = this.pivotDate;
    let datesBefore = []
    let nbBeforeDays = this.nbBeforeDays
    let nbAfterDays = this.nbAfterDays

    for (let i = 1; datesBefore.length < nbBeforeDays; i++) {
      let otherDate = pivotDate.clone().subtract(i, 'days')
      if (otherDate.isoWeekday() != 6 && otherDate.isoWeekday() != 7) {
        datesBefore.pushObject(otherDate);
      }
    }
    let datesAfter = []
    for (let i = 1; datesAfter.length < nbAfterDays; i++) {
      let otherDate = pivotDate.clone().add(i, 'days')
      if (otherDate.isoWeekday() != 6 && otherDate.isoWeekday() != 7) {
        datesAfter.pushObject(otherDate);
      }
    }

    let allDates = []
    allDates.pushObjects(datesBefore.reverse());
    allDates.pushObject(pivotDate.clone());
    allDates.pushObjects(datesAfter);

    return allDates;
  }),

  currentDate: computed(function () {
    return moment().format(dateParamFormat);
  }),

  dateBackward: computed('allDates.firstObject', 'nbOfDays', function () {
    let dateBackward

    if (this.get('allDates.length') > 2 ) {
      dateBackward = this.get('allDates.firstObject')
    } else {
      dateBackward = this.get('allDates.firstObject').subtract(1, 'days')
    }

    return dateBackward.format(dateParamFormat)
  }),

  dateForward: computed('allDates.lastObject', function () {
    let dateForward

    if (this.get('allDates.length') === 1 ) {
      dateForward = this.get('allDates.lastObject').add(1, 'days')
    } else {
      dateForward = this.get('allDates.lastObject')
    }

    return dateForward.format(dateParamFormat)
  }),

  calendarDays: computed('allDates.[]', 'consultationType.id', 'forceRefetchToggle', function () {
    let consultationTypeId = this.get('consultationType.id');
    if (!consultationTypeId) {
      return [];
    }

    let allDates = this.allDates;
    let displayHidden = this.displayHidden;

    let calendarDays = allSettled(allDates.map((date) => {
      return hashSettled({
        date: date,
        dutyPeriods: this._getDutyPeriods(date, consultationTypeId),
        appointments: this._getAppointments(date, consultationTypeId)
      })
    })).then((allDaysOfDutyPeriods) => {
      this._updateDataFetch();

      return allDaysOfDutyPeriods.map(function (promise) {
        if (promise.state === 'fulfilled') {
          let calendarDay = CalendarDay.create({
            displayHidden: displayHidden,
            date: promise.value.date.value
          });
          if (promise.value.dutyPeriods.state === 'fulfilled') {
            calendarDay.set('dutyPeriods', promise.value.dutyPeriods.value);
          }
          else {
            calendarDay.set('dutyPeriods', []);
          }
          if (promise.value.appointments.state === 'fulfilled') {
            calendarDay.set('appointments', promise.value.appointments.value);
          }
          else {
            calendarDay.set('appointments', []);
          }
          return calendarDay;
        }
      }).compact();
    });

    return DS.PromiseArray.create({
      promise: calendarDays
    });
  }),

  onDisplayHiddenChange: observer('displayHidden', function () {
    let displayHidden = this.displayHidden;

    this.calendarDays.forEach(calendarDay => calendarDay.set('displayHidden', displayHidden));
  }),

  dutyPeriodsByCalendarDay: computed('calendarDays.@each.dutyPeriods', function () {
    return this.calendarDays.mapBy('dutyPeriods');
  }),

  dutyPeriods: computed('dutyPeriodsByCalendarDay.[]', function () {
    return this.dutyPeriodsByCalendarDay.reduce(function (acc, dutyPeriods) {
      return acc.pushObjects(dutyPeriods.toArray());
    }, []);
  }),

  earliestStartTime: computed('dutyPeriods.@each.startsAt', function () {
    return this.dutyPeriods.map(function (dutyPeriod) {
      let startsAt = moment(dutyPeriod.get('startsAt'));
      let offset = startsAt.minutes() || 60;
      return startsAt.subtract(offset, 'minutes').format('HH:mm');
    }).sort().get('firstObject');
  }),

  latestEndTime: computed('dutyPeriods.@each.endsAt', function () {
    return this.dutyPeriods.map(function (dutyPeriod) {
      let endsAt = moment(dutyPeriod.get('endsAt'));
      let offset = 60 - endsAt.minutes();
      return endsAt.add(offset, 'minutes').format('HH:mm');
    }).sort().get('lastObject');
  }),

  users: computed('consultationType.assignedRoles.@each.id', function () {
    let assignedRoleIds = this.get('consultationType.assignedRoles').mapBy('id');
    let assignableUsers = this.store.findAll('user', {reload: true}).then((users) => {
      return users.reject((user) => !assignedRoleIds.includes(user.get('role.id')))
    });

    return DS.PromiseArray.create({
      promise: assignableUsers
    });
  }),

  onQueryParamsDidChange: on('queryParamsDidChange', function() {
    this.toggleProperty('forceRefetchToggle');
  }),

  _getDutyPeriods(date, consultationTypeId) {
    return this.store.query('duty-period', {
      filter: {
        consultation_type_id: consultationTypeId,
        scheduled_on: date.format(dateParamFormat)
      }
    });
  },

  _getAppointments(date, consultationTypeId) {
    return this.store.query('appointment', {
      filter: {
        consultation_type_id: consultationTypeId,
        scheduled_on: date.format(dateParamFormat)
      }
    });
  },

  actions: {
    goToCurrentDate() {
      this.transitionToRoute({ queryParams: { focusOn: this.currentDate }});
    },

    goBackward() {
      this.transitionToRoute({ queryParams: { focusOn: this.dateBackward }});
    },

    goForward() {
      this.transitionToRoute({ queryParams: { focusOn: this.dateForward }});
    },

    pickDate() {
      this.set('pickingDate', this.pivotDate.toDate())
    },

    stopPickingDate() {
      this.set('pickingDate', undefined);
    },

    goToPickedDate(pickedDate) {
      this.set('pickingDate', undefined);
      let pickedMoment = moment(pickedDate);

      this.transitionToRoute({ queryParams: { focusOn: pickedMoment.format(dateParamFormat) }});
    },

    updateDataFetch() {
      this._updateDataFetch();
    },
  },

  _updateDataFetch() {
    this.set('lastDataFetch', moment().toDate());
  }
});
