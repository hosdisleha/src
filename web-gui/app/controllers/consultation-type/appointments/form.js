/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import QueryParams from 'ember-parachute';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { on } from '@ember/object/evented';
import { reads, sort } from '@ember/object/computed';
import moment from 'moment';
import Changeset from 'ember-changeset';
import { w } from '@ember/string'
import DS from 'ember-data';

export const queryParams = {
  copyFromId: {
    defaultValue: undefined
  },
  postponedBy: {
    defaultValue: undefined
  },
  startsAt: {
    defaultValue: undefined,
    replace: true,
    serialize(value) {
      if (value) {
        return moment(value).toISOString();
      }
    },
    deserialize(value) {
      return moment(value);
    }
  }
};

export default Controller.extend(new QueryParams(queryParams).Mixin, {
  flashMessages: service(),
  currentUser: service(),

  setupCopyFromId: on('setup', function({ queryParams }) {
    if (queryParams.copyFromId) {
      this.store.findRecord('appointment', queryParams.copyFromId).then(previousAppointment => {
        let copyOptions;
        let commonAttributes = w('id onmlId startsAt');

        this.set('previousAppointment', previousAppointment)

        if (queryParams.postponedBy) {
          let workflowAttributes = w('cancelledAt previousAppointment previousAppointmentPostponedBy postponedBy originatingAppointment complementaryAppointments patientArrivedAt patientMissing certificate userInCharge patientReleasedAt attachedFiles')
          copyOptions = {
            ignoreAttributes: commonAttributes + workflowAttributes,
            overwrite: {
              previousAppointment: previousAppointment,
              previousAppointmentPostponedBy: queryParams.postponedBy,
              patient: previousAppointment.get('patient')
            },
          }
        }
        else {
          let consultationTypeRelatedAttributes = w('consultationSubtype consultationReason duration');
          copyOptions = {
            ignoreAttributes: commonAttributes + consultationTypeRelatedAttributes,
            overwrite: {consultationType: this.consultationType}
          }
        }
        previousAppointment.copy(false, copyOptions).then((appointmentCopy) => {
          this.set('appointment', appointmentCopy);
        })
      })
    } else {
      this.set('previousAppointment', undefined)
    }
  }),

  previousAppointmentSummary: computed('previousAppointment.typeAndDateTime', function() {
    return this.get('previousAppointment.typeAndDateTime')
  }),

  setupStartsAt: on('setup', function({ queryParams }) {
    if (queryParams.startsAt) {
      this.changeset.set('startsAt', queryParams.startsAt);
    }
  }),

  onStartsAtChange: on('queryParamsDidChange', function({ changed }) {
    if (changed.startsAt) {
      this.changeset.set('startsAt', changed.startsAt);
    }
  }),

  appointment: computed('model', {
    get(/* key */) {
      return this.model;
    },
    set(key, value) {
      return value;
    },
  }),

  consultationType: reads('appointment.consultationType'),

  changeset: computed('appointment', {
    get(/* key */) {
      return new Changeset(this.appointment);
    },
    set(key, value) {
      return value;
    },
  }),

  consultationTypes: computed('currentUser.consultationTypes', 'consultationType.id',  function () {
    return this.get('currentUser.consultationTypes').filter(type => type.get('id') != this.get('consultationType.id'));
  }),

  consultationTypesSorting: Object.freeze(['title']),
  sortedConsultationTypes: sort('consultationTypes', 'consultationTypesSorting'),
  newConsultationType: computed('shouldCreateOther', 'sortedConsultationTypes.firstObject', {
    get() {
      return this.shouldCreateOther && this.get('sortedConsultationTypes.firstObject');
    },

    set(key, value) {
      return value;
    }
  }),

  actions: {
    save(changeset, shouldCreateOther) {
      return changeset.save().then((appointment) => {
        appointment.set('previousAppointmentPostponedBy', null)
        this.onSave();
        this.flashMessages.success('Enregistrement réussi.')
        this.set('shouldCreateOther', shouldCreateOther);
        if (!shouldCreateOther) {
          this.set('previousAppointment', undefined)
          this._goBackToAppointments(changeset);
        }
      }).catch(() => {
        this.flashMessages.danger('Impossible de sauvegarder vos changements.')
      });
    },

    createOther() {
      let newConsultationTypeId = this.get('newConsultationType.id');
      this.set('newConsultationType', null);
      this.set('shouldCreateOther', false);

      this.transitionToRoute('consultation-type.appointments.new', newConsultationTypeId, {
        queryParams: {
          focusOn: moment(this.get('appointment.startsAt')).format('YYYY-MM-DD'),
          copyFromId: this.get('appointment.id'),
          startsAt: null
        }
      });
    },

    cancelCreateOther() {
      this.set('shouldCreateOther', false);
      this.set('previousAppointment', undefined)

      this._goBackToAppointments(this.appointment);
    },

    validate(/* { key, newValue, oldValue, changes, content } */) {
      return true
    }
  },

  _goBackToAppointments(appointment) {
    this.transitionToRoute('consultation-type.appointments', appointment.get('consultationType.id'), {
      queryParams: {
        focusOn: moment(appointment.get('startsAt')).format('YYYY-MM-DD')
      }
    });
  }
});
