/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { attr, belongsTo } from '@ember-data/model';
import Upload from './upload';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { equal, and } from '@ember/object/computed'
import { computed as awesomeComputed } from 'ember-awesome-macros';

export default Upload.extend({
  type: attr('string'),
  createdAt: attr('date'),

  session: service(),
  url: awesomeComputed('id', 'session.data.authenticated.access_token', function (id, access_token) {
    let adapter = this.store.adapterFor('application');
    let baseURL = adapter.buildURL(this.constructor.modelName, id);

    return baseURL + '/download?access_token=' + access_token;
  }),

  attachable: belongsTo('attachable', { inverse: 'attachedFiles' }),

  attachedToAppointment: equal('attachable.content.constructor.modelName', 'appointment'),
  appointment: and('attachedToAppointment', 'attachable'),

  attachedToFlowingConsultation: equal('attachable.content.constructor.modelName', 'flowing-consultation'),
  flowingConsultation: and('attachedToFlowingConsultation', 'attachable'),

  attachedToUser: equal('attachable.content.constructor.modelName', 'user'),
  user: and('attachedToUser', 'attachable'),

  attachedToCertificate: equal('attachable.content.constructor.modelName', 'certificate'),
  certificate: and('attachedToCertificate', 'attachable'),

  isStandard: equal('type', 'standard'),
  isMedicalContent: equal('type', 'medical-content'),
  isRedactedCertificate: equal('type', 'redacted-certificate'),
  isSignedCertificate: equal('type', 'signed-certificate'),
  isvalidatedCertificate: equal('type', 'validated-certificate'),
  isPrintedSignature: equal('type', 'printed-signature'),

  icon: computed('type', function() {
    switch (this.type) {
      case 'medical-content':
        return 'stethoscope'
      case 'validated-certificate':
        return 'file-alt'
      case 'signed-certificate':
        return 'certificate'
      default:
        return 'paperclip'
    }
  })

});
