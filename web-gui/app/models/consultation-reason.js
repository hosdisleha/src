/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo } from '@ember-data/model';
import { computed } from '@ember/object';
import moment from 'moment';
import { htmlSafe } from '@ember/string';

export default Model.extend({
  appointmentDuration: attr('duration', {
    defaultValue() {
      return moment.duration(60, 'minutes');
    }
  }),
  code: attr('string'),
  colour: attr('string'),
  title: attr('string'),
  consultationType: belongsTo('consultation-type'),

  appointmentDurationAsMinutes: computed('appointmentDuration', {
    get(/* key */) {
      return this.appointmentDuration.asMinutes();
    },
    set(key, value) {
      this.set('appointmentDuration',  moment.duration(parseInt(value), 'minutes'));
      return value;
    }
  }),

  titleWithCode: computed('title', 'code', function () {
    return `${this.title} (${this.code})`;
  }),

  cssColor: computed('colour', function () {
    let colour = this.colour;

    return colour ? htmlSafe('color: ' + colour) : null;
  }),

  cssBackgroundColor: computed('colour', function () {
    let colour = this.colour;

    return colour ? htmlSafe('background-color: ' + colour) : null;
  }),

});
