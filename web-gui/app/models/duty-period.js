/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { computed } from '@ember/object';
import { notEmpty } from '@ember/object/computed';
import moment from 'moment';

export default Model.extend({
  startsAt: attr('date'),
  endsAt: attr('date'),
  consultationType: belongsTo('consultation-type'),
  assignees: hasMany('user'),

  timeRange: computed('startsAt', 'endsAt', function () {
    let startTime = moment(this.startsAt).format('HH:mm');
    let endTime = moment(this.endsAt).format('HH:mm');
    return [startTime, endTime].join(' - ')
  }),

  assigneesNames: computed('assignees.@each.abridgedName', function () {
    return this.assignees.mapBy('abridgedName').join(', ') || '∅';
  }),

  assigned: notEmpty('assignees'),

  color: computed('assigned', function () {
    return this.assigned ? 'mediumseagreen' : 'gray';
  })
});
