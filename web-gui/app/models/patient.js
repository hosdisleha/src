/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { dateFormat } from '../constants';

export default class PatientModel extends Model {
  @attr('string') gender;
  @attr('string') firstName;
  @attr('string') lastName;
  @attr('dateOnly') birthdate;
  @attr('number') birthYear;
  @attr('string') phoneNumbers;

  @belongsTo('patient', {inverse: null}) duplicatedPatient;
  @belongsTo('patient', {inverse: null}) probablyDuplicatedPatient;

  @hasMany('appointment') appointments;
  @hasMany('flowingConsultations') flowingConsultations;
  @hasMany('log', { inverse: 'loggable' }) logs;

  @service intl;

  get abbreviatedGender() {
    return this.intl.t('patient.gender.abbreviated.' + this.gender);
  }

  get fullName() {
    let result = [];

    let lastName = this.lastName;
    if (lastName) {
      result.push(lastName.toUpperCase());
    }

    let firstName = this.firstName;
    if (firstName) {
      result.push(firstName[0].toUpperCase() + firstName.slice(1).toLowerCase());
    }

    return result.join(' ');
  }

  get humanGender() {
    return this.intl.t('patient.gender.' + this.gender);
  }

  get humanBirth() {
    if (this.birthdate) {
      return moment(this.birthdate).format(dateFormat)
    }
    if (this.birthYear) {
      return this.birthYear.toString()
    }
    return ''
  }

  get fullMatcher() {
    if (this.id === null) { return null }

    return `${this.fullName} - ${this.humanBirth}  (${this.humanGender})`
  }

  get humanHistoryCount() {
    return this.intl.t('patient.history.count', {nb: this.appointments.length + this.flowingConsultations.length});
  }

  ageAtMoment(someDate) {
    if (someDate) {
      let someMoment = moment(someDate);

      if (this.birthdate) {
        let birthMoment = moment(this.birthdate);
        let age = someMoment.diff(birthMoment, 'years');

        return this.intl.t('patient.age', {nb: age});
      }

      if (this.birthYear) {
        let momentYear = someMoment.year();
        let age = momentYear - parseInt(this.birthYear);

        if (age < 2) {
          return "1 an ou moins…"
        }
        else {
          return `${age - 1}-${age} ans`;
        }
      }
    }

    return '';
  }
}
