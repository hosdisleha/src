/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import EmberObject from '@ember/object';
import {
  and,
  array,
  collect,
  nor,
  not,
  or,
  raw,
} from 'ember-awesome-macros';

export default EmberObject.extend({
  appointment: undefined,
  endOfLife: or('appointment.isPostponed', 'appointment.certificateDelivered'),
  isAlive: not('endOfLife'),
  stillScheduled: not('appointment.isCancelled'),

  calendarTransitions: array.filter(collect(
    'cancel', 'uncancel', 'postpone', 'unpostpone'
  ), (item) => item),
  appointementTransitions: array.filter(collect(
    'acknowledgePatientNoShow', 'unacknowledgePatientNoShow',
    'acknowledgePatientArrived', 'unacknowledgePatientArrived',
    'acknowledgeRequisition', 'unacknowledgeRequisition',
    'takeCharge', 'resignCharge'
  ),(item) => item),
  certificateTransitions: array.filter(collect(
    'editCertificate', 'downloadCertificate',
    'acknowledgeCertificateDelivered', 'unacknowledgeCertificateDelivered'
  ), (item) => item),

  transitionCategories: collect('calendarTransitions', 'appointementTransitions', 'certificateTransitions'),

  cancel: and('isAlive', not('appointment.isCancelled'), raw('cancel-transition')),
  uncancel: and('isAlive', 'appointment.isCancelled', raw('uncancel-transition')),
  postpone: and('isAlive', raw('postpone-transition')),
  unpostpone: and('appointment.isPostponed', raw('unpostpone-transition')),

  acknowledgePatientArrived: and('stillScheduled', nor('appointment.patientArrived', 'appointment.patientMissing'), raw('acknowledge-patient-arrived-transition')),
  unacknowledgePatientArrived: and('stillScheduled', 'appointment.patientArrived', raw('acknowledge-patient-arrived-transition')),
  acknowledgePatientNoShow: and('stillScheduled', nor('appointment.patientArrived', 'appointment.patientMissing'), raw('acknowledge-patient-noshow-transition')),
  unacknowledgePatientNoShow: and('stillScheduled', 'appointment.patientMissing', raw('acknowledge-patient-noshow-transition')),
  acknowledgeRequisition: and('stillScheduled', not('appointment.requisitionReceived'), raw('acknowledge-requisition-transition')),
  unacknowledgeRequisition: and('stillScheduled', 'appointment.requisitionReceived', raw('acknowledge-requisition-transition')),
  takeCharge: and('stillScheduled', 'appointment.patientArrived', not('appointment.takenInCharge'), raw('take-charge-transition')),
  resignCharge: and('stillScheduled', 'appointment.takenInCharge', raw('take-charge-transition')),

  editCertificate: and('stillScheduled', 'appointment.takenInCharge', raw('edit-certificate-transition')),
  downloadCertificate: and('stillScheduled', 'appointment.certificateValidated', raw('download-certificate-transition')),
  acknowledgeCertificateDelivered: and('stillScheduled', raw('acknowledge-delivery-transition')),
  unacknowledgeCertificateDelivered: and(not('acknowledgeCertificateDelivered'), raw('acknowledge-delivery-transition'))
});
