/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
export default [
  {
    "id": "111",
    "appointmentDuration": "PT1H",
    "code": "VV",
    "colour": "#00AFDF",
    "title": "Violence volontaire",
    "consultationTypeId": "11"
  },
  {
    "id": "112",
    "appointmentDuration": "PT45M",
    "code": "AS",
    "colour": "#00DFAF",
    "title": "Agression sexuelle",
    "consultationTypeId": "11"
  }
];
