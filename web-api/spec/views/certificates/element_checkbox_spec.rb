# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/element_checkbox", type: :view do

  before(:all) do
    appointment = create(:appointment, :with_certificate)
    appointment.certificate.update_attribute(:data, { "question-checkbox-Comment" => "quatre" })
    assign(:appointment, appointment)
  end

  describe "regarding simple choices" do

    let(:element) {
      {
        "type" => "checkbox",
        "name" => "question-checkbox",
        "title" => "QCMRM",
        "choices" => [
          "un",
          "deux",
          "trois"
        ]
      }
    }

    describe "without data" do
      it "renders nothing" do
        # Given
        # When
        render partial: "json_certificates/element_checkbox", locals: { element: element, data: nil}
        # Then
        expect(rendered).to be_empty
      end
    end

    describe "with data" do
      it "renders a dl with the title as dt and the choice texts as dd" do
        # Given
        data = [ "un", "trois" ]
        expected = "<dl><dt>QCMRM</dt><dd>un, trois</dd></dl>"
        # When
        render partial: "json_certificates/element_checkbox", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end

    describe "with data and other choice" do
      it "renders a dl with the title as dt and the choice texts dd" do
        # Given
        data = [ "un", "trois", "other" ]
        expected = "<dl><dt>QCMRM</dt><dd>un, trois, quatre</dd></dl>"
        # When
        render partial: "json_certificates/element_checkbox", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end
  end

  describe "regarding complex choices" do

    let(:element) {
      {
        "type" => "checkbox",
        "name" => "question-checkbox",
        "title" => "QCMRM",
        "choices" => [
          {
            "value" => "one",
            "text" => "un"
          },
          {
            "value" => "two",
            "text" => "deux"
          },
          {
            "value" => "three",
            "text" => "trois"
          }
        ]
      }
    }

    describe "without data" do
      it "renders nothing" do
        # Given
        # When
        render partial: "json_certificates/element_checkbox", locals: { element: element, data: nil}
        # Then
        expect(rendered).to be_empty
      end
    end

    describe "with data" do
      it "renders a dl with the title as dt and the choice texts dd" do
        # Given
        data = [ "one", "three" ]
        expected = "<dl><dt>QCMRM</dt><dd>un, trois</dd></dl>"
        # When
        render partial: "json_certificates/element_checkbox", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end

    describe "with data and other choice" do
      it "renders a dl with the title as dt and the choice texts dd" do
        # Given
        data = [ "one", "three", "other" ]
        expected = "<dl><dt>QCMRM</dt><dd>un, trois, quatre</dd></dl>"
        # When
        render partial: "json_certificates/element_checkbox", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end
  end
end
