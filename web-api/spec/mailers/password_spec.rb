# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe PasswordMailer, type: :mailer do

  let(:user) { create(:user, identifier: 'username') }

  describe "created" do
    subject { PasswordMailer.created(user) }

    it 'renders the headers' do
      expect(subject.subject).to match('[Filaé - TEST] username, votre compte a été créé.')
      expect(subject.to).to eq([user.email])
      expect(subject.from).to eq(['mailer@filae.tld'])
    end

    it 'renders the body' do
      expect(subject.body.encoded).to match(user.identifier)
      expect(subject.body.encoded).to match(user.password)
    end
  end

  describe "reset" do
    subject { PasswordMailer.reset(user) }

    it 'renders the headers' do
      expect(subject.subject).to match('[Filaé - TEST] username, votre mot de passe a été réinitialisé.')
      expect(subject.to).to eq([user.email])
      expect(subject.from).to eq(['mailer@filae.tld'])
    end

    it 'renders the body' do
      expect(subject.body.encoded).to match(user.password)
    end
  end
end
