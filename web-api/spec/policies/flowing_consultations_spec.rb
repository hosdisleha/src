# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding flowing consultations" do |role|
  it { is_expected.to permit_actions([:index, :show]) }
  it { is_expected.to forbid_actions([:create, :update]) }
end

describe FlowingConsultationPolicy do
  subject { described_class.new(user, flowing_consultation) }

  let(:flowing_consultation) { create :flowing_consultation }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_actions([:index, :show, :create, :update]) }
  end

  [
    :administrator,
    :physician,
    :nurse,
    :secretary,
    :switchboard_operator
  ].each do |role|
    context "as a(n) #{role}" do
      let(:user) { create :user, role }

      it { is_expected.to permit_actions([:index, :show, :create, :update]) }
    end
  end

  context "concerning an archived flowing consultation" do

    before { allow(flowing_consultation).to receive(:archived?).and_return(true) }

    [
      :administrator,
      :nurse,
      :secretary
    ].each do |role|
      context "as a(n) #{role}" do
        let(:user) { create :user, role }

        it { is_expected.to permit_action(:update )}
      end
    end

    [
      :physician,
      :switchboard_operator,
      :association,
      :psychiatrist,
      :psychiatrist_expert,
      :psychologist,
      :psychologist_expert
    ].each do |role|
      context "as a(n) #{role}" do
        let(:user) { create :user, role }

        it { is_expected.to forbid_action(:update) }
      end
    end
  end

  [
    :association,
    :psychiatrist,
    :psychologist
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user regarding flowing consultations" do
        let(:user) { create :user, role }
      end
    end
  end
end
