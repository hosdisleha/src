# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "Flowing consultations", type: :request do

  let!(:intervention_domain) { create :intervention_domain, id: 42 }
  let!(:patient) { create :patient, id: 42 }

  context "as an administrator" do

    before { connected_as_administrator }

    let!(:consultation_type) { create :consultation_type, id: 11, intervention_domain: intervention_domain }
    let!(:calling_institution) { create :calling_institution, id: 11 }

    describe "POST /flowing_consultations" do

      let(:consultation_data) {
        {
          "data": {
            "type": "flowing_consultations",
            "attributes": {
              "called_at": "2018-05-22T03:37Z",
              "officer_name": "Inspecter Harry Callahan",
              "officer_phone": "0647484940",
              "officer_email": "hcallahan@filae.tld",
              "officer_lambda_user": false,
              "comment": ""
            },
            "relationships": {
              "intervention_domain": {
                "data": {
                  "type": "intervention_domains",
                  "id": "42"
                }
              },
              "consultation_type": {
                "data": {
                  "type": "consultation_types",
                  "id": "11"
                }
              },
              "calling_institution": {
                "data": {
                  "type": "calling_institutions",
                  "id": "11"
                }
              }
            }
          }
        }
      }

      it "creates a new flowing consultation" do
        # Given
        expect {
          # When
          post "/flowing_consultations", params: consultation_data.to_json, headers: headers
          # Then
        }.to change(FlowingConsultation, :count).by(1)
        expect(response).to have_http_status(201)
      end

      it "creates a new officer" do
        # Given
        expect {
          # When
          post "/flowing_consultations", params: consultation_data.to_json, headers: headers
          # Then
        }.to change(Officer, :count).by(1)
        expect(response).to have_http_status(201)
      end

      describe "regarding existing patient" do
        it "uses her" do
          # Given
          consultation_data[:data][:relationships].merge!({
              "patient": {
                "data": { "type": "patients", "id": "42" }
              }
          })
          expect {
            # When
            post "/flowing_consultations", params: consultation_data.to_json, headers: headers
            # Then
          }.not_to change(Patient, :count)
          expect(response).to have_http_status(201)
        end
      end

      describe "regarding a new patient" do
        it "creates her" do
          # Given
          consultation_data[:data][:attributes].merge!({
            "patient_first_name": "Mister",
            "patient_last_name": "Scorpion",
            "patient_birth_year": 1971,
            "patient_gender": "man",
          })
          expect {
            # When
            post "/flowing_consultations", params: consultation_data.to_json, headers: headers
            # Then
          }.to change(Patient, :count).by(1)
          expect(response).to have_http_status(201)
        end
      end

      it "creates a 'created' log job" do
        # Given
        expected_action = Log::Action.new("created")
        expect {
          # When
          post "/flowing_consultations", params: consultation_data.to_json, headers: headers
          # Then
        }.to have_enqueued_job(LogJob)
        compare_log_event_to([expected_action])
      end
    end

    describe "PATCH /flowing_consultations/12" do

      let(:officer) { create :officer }
      let!(:other_patient) { create :patient, id: 49 }
      let!(:assignee) { create :user }
      let!(:new_consultation_type) { create :consultation_type, id: 42 }
      let!(:new_calling_institution) { create :calling_institution, id: 42 }
      let!(:new_assignee) { create :user, id: 42 }
      let!(:flowing_consultation) {
        create :flowing_consultation,
          id: 12,
          officer: officer,
          patient: patient,
          consultation_type: consultation_type,
          calling_institution: calling_institution,
          assignee: assignee
      }

      let(:consultation_data) {
        {
          "data": {
            "type": "flowing_consultations",
            "id": "12",
            "attributes": {
              "called_at": "2018-05-21T23:42Z",
              "officer_name": "Commissaire Jules Maigret",
              "officer_phone": "0617181910",
              "officer_email": "jmaigret@filae.tld",
              "comment": "Lorem Ipsum",
              "assigned_at": "2018-05-21T23:52Z",
              "validated_at": "2018-05-22T00:52Z",
              "cancelled_at": "2018-05-22T03:17Z",
              "cancelled_by": "custody-ended"
            },
            "relationships": {
              "consultation_type": {
                "data": {
                  "type": "consultation_types",
                  "id": "42"
                }
              },
              "calling_institution": {
                "data": {
                  "type": "calling_institutions",
                  "id": "42"
                }
              },
              "assignee": {
                "data": {
                  "type": "users",
                  "id": "42"
                }
              },
              "patient": {
                "data": {
                  "type": "patients",
                  "id": "49"
                }
              }
            }
          }
        }
      }

      it "updates the flowing consultation attributes" do
        # Given
        expect {
          # When
          patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
          # Then
        }.to_not change(Patient, :count)
        expect(response).to have_http_status(200)
        flowing_consultation.reload
        expect(flowing_consultation.comment).to eq "Lorem Ipsum"
        expect(flowing_consultation.called_at.utc).to eq DateTime.new(2018,5,21,23,42).utc
        expect(flowing_consultation.assigned_at.utc).to eq DateTime.new(2018,5,21,23,52).utc
        expect(flowing_consultation.validated_at.utc).to eq DateTime.new(2018,5,22,00,52).utc
        expect(flowing_consultation.cancelled_at.utc).to eq DateTime.new(2018,5,22,03,17).utc
      end

      it "updates the officer attributes" do
        # Given
        expect {
          # When
          patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
          # Then
        }.to_not change(Officer, :count)
        expect(response).to have_http_status(200)
        flowing_consultation.reload
        officer = flowing_consultation.officer
        expect(officer.name).to eq "Commissaire Jules Maigret"
        expect(officer.phone).to eq "0617181910"
        expect(officer.email).to eq "jmaigret@filae.tld"
      end

      it "changes the patient" do
        # Given
        expect {
          # When
          patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
          # Then
        }.to_not change(Patient, :count)
        expect(response).to have_http_status(200)
        flowing_consultation.reload
        expect(flowing_consultation.patient.id).to eq 49
      end

      it "updates the consultation type" do
        # Given
        # When
        patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
        # Then
        expect(response).to have_http_status(200)
        flowing_consultation.reload
        expect(flowing_consultation.consultation_type).to eq new_consultation_type
      end

      it "updates the calling institution" do
        # given
        # when
        patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
        # then
        expect(response).to have_http_status(200)
        flowing_consultation.reload
        expect(flowing_consultation.calling_institution).to eq new_calling_institution
      end

      it "updates the assignee" do
        # given
        # when
        patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
        # then
        expect(response).to have_http_status(200)
        flowing_consultation.reload
        expect(flowing_consultation.assignee).to eq new_assignee
      end

      describe "regarding assignment" do

        before do
          consultation_data[:data][:attributes][:validated_at] = nil
          consultation_data[:data][:attributes][:cancelled_at] = nil
        end

        it "creates an 'assigned' log job on assignement" do
          # Given
          flowing_consultation.update_attribute(:assignee, nil)
          expected_action = Log::Action.new("assigned")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates an 'unassigned' log job on unassignement" do
          # Given
          consultation_data[:data][:relationships][:assignee][:data] = nil
          expected_action = Log::Action.new("unassigned")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates a 'reassigned' log job on reassignement" do
          # Given
          expected_action = Log::Action.new("reassigned")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end
      end

      describe "regarding validation" do

        before do
          consultation_data[:data][:relationships][:assignee][:data][:id] = flowing_consultation.assignee.id.to_s
          consultation_data[:data][:attributes][:cancelled_at] = nil
        end

        it "creates an 'validated' log job on validation" do
          # Given
          flowing_consultation.update_attribute(:validated_at, nil)
          expected_action = Log::Action.new("validated")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates an 'unvalidated' log job on unvalidation" do
          # Given
          flowing_consultation.update_attribute(:validated_at, Time.zone.now)
          consultation_data[:data][:attributes][:validated_at] = nil
          expected_action = Log::Action.new("unvalidated")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates a 'revalidated' log job when validation datetime changed" do
          # Given
          flowing_consultation.update_attribute(:validated_at, Time.zone.now)
          expected_action = Log::Action.new("revalidated")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end
      end

      describe "regarding cancellation" do

        before do
          consultation_data[:data][:relationships][:assignee][:data][:id] = flowing_consultation.assignee.id.to_s
          consultation_data[:data][:attributes][:validated_at] = nil
        end

        it "creates an 'cancelled' log job on cancellation" do
          # Given
          flowing_consultation.update_attribute(:cancelled_at, nil)
          expected_action = Log::Action.new({ action: "cancelled", source: "custody-ended"})
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates an 'uncancelled' log job on uncancellation" do
          # Given
          flowing_consultation.update_attribute(:cancelled_at, Time.zone.now)
          consultation_data[:data][:attributes][:cancelled_at] = nil
          expected_action = Log::Action.new("uncancelled")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates a 'recancelled' log job when cancellation datetime changed" do
          # Given
          flowing_consultation.update_attribute(:cancelled_at, Time.zone.now)
          expected_action = Log::Action.new("recancelled")
          expect {
            # When
            patch "/flowing_consultations/12", params: consultation_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end
      end
    end
  end

  describe "GET /flowing_consultations" do

    [
      :administrator,
      :physician,
      :nurse,
      :secretary,
    ].each do |role|

      context "as a #{role}" do

        before { send "connected_as_#{role}" }

        it "can access active flowing consultations" do
          # Given
          # When
          get "/flowing_consultations?filter[intervention_domain_id]=42&filter[state]=active", headers: headers(role)
          # Then
          expect(response).to have_http_status(200)
        end

        it "can access archived flowing consultation" do
          # Given
          # When
          get "/flowing_consultations?filter[intervention_domain_id]=42&filter[state]=archive&sort=-called_at", headers: headers(role)
          # Then
          expect(response).to have_http_status(200)
        end
      end
    end

    [
      :association,
      :psychiatrist,
      :psychiatrist_expert,
      :psychologist,
      :psychologist_expert,
      :switchboard_operator
    ].each do |role|

      context "as a #{role}" do

        before { send "connected_as_#{role}" }

        it "can access active flowing consultations" do
          # Given
          # When
          get "/flowing_consultations?filter[intervention_domain_id]=42&filter[state]=active", headers: headers(role)
          # Then
          expect(response).to have_http_status(200)
        end

        it "can't access archived flowing consultation" do
          # Given
          # When
          get "/flowing_consultations?filter[intervention_domain_id]=42&filter[state]=archive&sort=-called_at", headers: headers(role)
          # Then
          expect(response).to have_http_status(403)
        end
      end
    end
  end
end
