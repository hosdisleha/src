# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
module RequestMacros

  def headers(role = :administrator)
    {
      "ACCEPT" => "application/vnd.api+json",
      "Authorization" => "Bearer my_secret_token_#{role}",
      "Content-Type" => "application/vnd.api+json",
    }
  end

  def visitor_headers
    {
      "ACCEPT" => "application/vnd.api+json",
      "Content-Type" => "application/vnd.api+json",
    }
  end

  def compare_log_event_to(actions)
    expect(ActiveJob::Base.queue_adapter.enqueued_jobs.last[:args][3]).to eq actions.map(&:to_json)
  end

  def method_missing(m, *args, &block)
    if role_title = m.to_s.split(/connected_as_/)[1]
      connect_as(role_title)
    else
      super
    end
  end

  private


  def connect_as(role_title)
    role = Role.find_or_create_by!(title: role_title)
    user = create :user, role: role, identifier: role_title, password: role_title
    Doorkeeper::AccessToken
      .create!(resource_owner_id: user.id)
      .update_attribute(:token, "my_secret_token_#{role_title}")
    return user
  end
end
