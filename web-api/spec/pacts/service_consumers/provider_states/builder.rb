# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Builder

  def self.role_id sym
    Role.find_by(title: sym).id
  end

  def self.role_ids syms
    syms.map { |sym| role_id(sym) }
  end

  USERS = [
    { id: 1, identifier: "administrator", password: "administrator", role_id: role_id(:administrator) },
    { id: 2, identifier: "physician", password: "physician", role_id: role_id(:physician) },
    { id: 3, identifier: "inactive-user", password: "physician", role_id: role_id(:physician), disabled_at: 2.weeks.ago }
  ]

  INTERVENTION_DOMAINS = [
    { id: 1, title: 'Victimologie', role_ids: role_ids(%i[administrator association nurse physician psychiatrist psychologist secretary]) },
    { id: 2, title: 'Antenne', role_ids: role_ids(%i[administrator nurse physician secretary switchboard_operator]) }
  ]

  CONSULTATION_TYPES = [
    { id: 11, title: 'Médicale', icon_name: "stethoscope", intervention_domain_id: 1, role_ids: role_ids(%i[administrator nurse physician]), assigned_role_ids: role_ids(%i[physician]) },
    { id: 12, title: 'Psychiatrique', icon_name: "psycho", intervention_domain_id: 1, role_ids: role_ids(%i[administrator nurse psychiatrist]), assigned_role_ids: role_ids(%i[psychiatrist]) },
    { id: 21, intervention_domain_id: 2 },
    { id: 22, intervention_domain_id: 2 }
  ]

  CONSULTATION_REASONS = [
    { id: 111, consultation_type_id: 11},
    { id: 112, consultation_type_id: 11},
    { id: 122, consultation_type_id: 12}
  ]

  CONSULTATION_SUBTYPES = [
    { id: 121, consultation_type_id: 12},
    { id: 122, consultation_type_id: 12}
  ]

  SCHEDULES = [
    { id: 11, intervention_domain_id: 1, activated_on: "2018-05-19" },
    { id: 12, intervention_domain_id: 1, activated_on: "2018-05-30" }
  ]

  TIME_SLOTS = [
    { id: 11111, schedule_id: 11, consultation_type_id: 11, week_day: 1 },
    { id: 11112, schedule_id: 11, consultation_type_id: 11, week_day: 1 },
    { id: 11113, schedule_id: 11, consultation_type_id: 11, week_day: 3 },
    { id: 11121, schedule_id: 11, consultation_type_id: 12 }
  ]

  DUTY_PERIODS = [
    { id: 1105211, time_slot_id: 11111, scheduled_on: "2018-05-21", assignee_ids: [2]},
    { id: 1105212, time_slot_id: 11112, scheduled_on: "2018-05-21"}
  ]

  CALLING_AREAS = [
    { id: 1 },
    { id: 2 }
  ]

  CALLING_INSTITUTIONS = [
    { id: 11, calling_area_id: 1 },
    { id: 12, calling_area_id: 1 }
  ]

  OFFICERS = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5, lambda_user: true }
  ]

  PATIENTS = [
    { id: 1, first_name: "Cindy", last_name: "Rella", birthdate: "2008-05-19" },
    { id: 2, first_name: "Jérôme", last_name: "Martinaud", birth_year: 1981 }
  ]

  FLOWING_CONSULTATIONS = [
    { id: 11, intervention_domain_id: 2, consultation_type_id: 21, calling_institution_id: 11, officer_id: 1, patient_id: 2, assignee_id: 2, assigned_at: Time.zone.now, validated_at: Time.zone.now },
    { id: 12, intervention_domain_id: 2, consultation_type_id: 21, calling_institution_id: 11, officer_id: 2, patient_id: 2, assignee_id: 2, assigned_at: Time.zone.now },
    { id: 13, intervention_domain_id: 2, consultation_type_id: 22, calling_institution_id: 11, officer_id: 3, patient_id: 2, concerns_minor: true },
    { id: 14, intervention_domain_id: 2, consultation_type_id: 21, calling_institution_id: 11, officer_id: 4, patient_id: 2, cancelled_at: Time.zone.now, cancelled_by: 'custody-ended' },
    { id: 15, intervention_domain_id: 2, consultation_type_id: 21, calling_institution_id: 11, officer_id: 4, patient_id: 2, called_at: Time.zone.now - 20.hours, cancelled_at: Time.zone.now - 13.hours, cancelled_by: 'custody-ended' },
    { id: 16, intervention_domain_id: 2, consultation_type_id: 21, calling_institution_id: 11, officer_id: 1, patient_id: 2, assignee_id: 2, called_at: Time.zone.now - 22.hours, assigned_at: Time.zone.now - 15.hours, validated_at: Time.zone.now - 13.hours  },
    { id: 17, intervention_domain_id: 2, consultation_type_id: nil, officer_id: 5, patient_id: 2, assignee_id: 2, called_at: Time.zone.now - 23.hours, assigned_at: Time.zone.now - 15.hours, validated_at: Time.zone.now - 14.hours, comment: "Primum non nocere"},
    { id: 18, intervention_domain_id: 2, consultation_type_id: nil, officer_id: 5, patient_id: 2, assignee_id: 2, called_at: Time.zone.now - 22.hours, assigned_at: Time.zone.now - 15.hours, comment: "Primum non nocere" },
    { id: 19, intervention_domain_id: 1, consultation_type_id: 11, calling_institution_id: 11, officer_id: 1, patient_id: 1 }
  ]

  APPOINTMENTS = [
    { id: 1105211, consultation_type_id: 11, calling_institution_id: 11, consultation_reason_id: 111, officer_id: 1, patient_id: 1, starts_at: "2018-05-21T06:00Z", circumstances: "", user_in_charge_id: 2 },
    { id: 1105212, consultation_type_id: 11, calling_institution_id: 11, consultation_reason_id: 112, officer_id: 1, patient_id: 1, starts_at: "2018-05-21T06:30Z" },
    { id: 1205211, consultation_type_id: 12, consultation_subtype_id: 122, calling_institution_id: 11, consultation_reason_id: 122, officer_id: 1, patient_id: 1, starts_at: "2018-05-21T07:30Z" }
  ]

  CERTIFICATE_TEMPLATES = [
    { id: 1 }
  ]

  MASTER_DOCUMENTS = [
    { id: 1, attachable_type: 'CertificateTemplate', attachable_id: 1 }
  ]

  ATTACHED_FILES = [
    { id: 1, attachable_id: 1105211, attachable_type: "Appointment", type: "AttachedFile::Standard" },
    { id: 2, attachable_id: 11, attachable_type: "FlowingConsultation", type: "AttachedFile::Standard" },
    { id: 3, attachable_id: 1, attachable_type: "Certificate", type: "AttachedFile::SignedCertificate" }
  ]

  MEDICAL_FILES = [
    { id: 1 }
  ]

  CERTIFICATES = [
    { id: 11052111, certificatable_id: 1105211, certificatable_type: 'Appointment' },
    { id: 11052121, certificatable_id: 1105212, certificatable_type: 'Appointment' },
    { id: 12052111, certificatable_id: 1205211, certificatable_type: 'Appointment' },
    { id: 1, certificatable_id: 11, certificatable_type: 'FlowingConsultation', validated_at: "2019-10-07T09:37Z", validator_id: 1, checksum: SecureRandom.uuid, delivered_at: "2019-10-07T10:37Z" }
  ]

  def self.create_all *models # e.g. InterventionDomain, ConsultationType
    self.tap do
      models.each do |model| # e.g. InterventionDomain
        table_name = model.table_name # e.g. "intervention_domains"
        constant = self.const_get(table_name.upcase) # e.g. INTERVENTION_DOMAINS
        factory = table_name.singularize # e.g. "intervention_domain"

        while model.count < constant.count
          create factory, constant[model.count]
        end
        ActiveRecord::Base.connection.reset_pk_sequence!(table_name)
      end
    end
  end

end
