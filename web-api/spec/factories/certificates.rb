# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :certificate do
    questionnaire {
      {
        "pages": [
          {
            "name": 42,
            "elements": [
              {
                "name": "circumstances-ref",
                "title": "Circonstances",
                "description": "Ce qu'il s'est passé",
                "type": "comment"
              }
            ]
          }
        ]
      }
    }
    data {
      {
        "circumstances-ref": "Sortie de discothèque"
      }
    }
    delivered_at { "" }
    validated_at { "" }

    of_appointment

    trait :validated do
      validated_at { Time.zone.now }
      association :validator, factory: :user
      checksum { SecureRandom.uuid }
    end

    trait :of_appointment do
      association :certificatable, factory: :appointment
    end

    trait :of_flowing_consultation do
      association :certificatable, factory: :flowing_consultation
    end
  end
end
