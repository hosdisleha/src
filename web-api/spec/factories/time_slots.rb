# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :time_slot do
    week_day { Faker::Number.between(from: 1, to: 7) }
    starts_at { random_time_within_current_day }
    ends_at { random_time_within_current_day }
    schedule
    consultation_type
  end
end

def random_time_within_current_day
  Faker::Time.between(from: Time.zone.now.beginning_of_day, to: Time.zone.now.end_of_day)
end
