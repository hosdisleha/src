# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :medical_file do
    address { Faker::Address.full_address }
    chaperon_name { Faker::Name.name }
    chaperon_quality { Faker::Relationship.familial }
    phone_number { Faker::PhoneNumber.phone_number }
    placed { Faker::Boolean.boolean }
    actor_name { Faker::Name.name }
    obs_nurse { Faker::Lorem.paragraph }
    obs_psychologist { Faker::Lorem.paragraph }
    obs_general { Faker::Lorem.paragraph }
    wrist_xray { Faker::Boolean.boolean }
    teeth_xray { Faker::Boolean.boolean }
    clavicle_xray { Faker::Boolean.boolean }
    std_test { Faker::Boolean.boolean }
    pre_therapy_test { Faker::Boolean.boolean }
    cga_test { Faker::Boolean.boolean }
    dna_test { Faker::Boolean.boolean }
    chemical_submission { Faker::Boolean.boolean }
    triple_therapy { Faker::Boolean.boolean }
    dressing { Faker::Boolean.boolean }
    psychological_impact_requested { Faker::Boolean.boolean }
    medical_obs { Faker::Lorem.paragraph }
    case_history { Faker::Lorem.paragraph }
    background { Faker::Lorem.paragraph }
    treatment { Faker::Lorem.paragraph }
    allergy { Faker::Lorem.paragraph }
    clinical_examination { Faker::Lorem.paragraph }
    complementary_info { Faker::Lorem.paragraph }

    appointment
  end
end
