# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :appointment do
    starts_at { Time.zone.now }
    duration { "PT30M" }
    official_report { "123456" }
    officer { nil }
    patient { nil }
    patient_phone { Faker::PhoneNumber.phone_number }
    circumstances { nil }
    comment { nil }
    consultation_type
    consultation_reason { nil }
    calling_institution { nil }
    consultation_subtype { nil }

    trait :with_patient do
      patient
    end

    trait :with_certificate do
      certificate
    end

    trait :with_validated_certificate do
      association :certificate, :validated
    end
  end
end
