require 'rails_helper'

RSpec.describe Log::Action, type: :model do

  context "initialization" do

    it "from a string" do
      # Given
      action = "created"
      # When
      log_action = described_class.new(action)
      # Then
      expect(log_action.name).to eq "created"
      expect(log_action.source).to be nil
      expect(log_action.changes).to be nil
    end

    it "from a Hash" do
      # Given
      changes = { birthdate: [2000,2001]}
      action = { action: "postponed", source: "umj", changes: changes }
      # When
      log_action = described_class.new(action)
      # Then
      expect(log_action.name).to eq "postponed"
      expect(log_action.source).to eq "umj"
      expect(log_action.changes).to eq changes
    end

  end

  describe 'id' do

    it 'without log' do
      # Given
      action = 'created'
      # When
      log_action = described_class.new(action)
      # Then
      expect(log_action.id).to be nil
    end

    it 'with log' do
      # Given
      action = 'created'
      log = create(:log)
      # When
      log_action = described_class.new(action, log)
      # Then
      expect(log_action.id).to start_with(log.what_id)
      expect(log_action.id).to match("-#{log.id.to_s}-")
      expect(log_action.id).to end_with(action)
    end

  end

  context "#to_json generates a String containing" do
    it "the name, the source and the changes" do
      # Given
      log_action = described_class.new({ action: "postponed", source: "umj", changes: { birthdate: [2000,2001] } })
      # When
      result = log_action.to_json
      # Then
      expect(result).to eq '{"action":"postponed","source":"umj","changes":{"birthdate":[2000,2001]}}'
    end

    it "only the name if source is missing" do
      # Given
      log_action = described_class.new("created")
      # When
      result = log_action.to_json
      # Then
      expect(result).to eq '{"action":"created"}'
    end
  end
end
