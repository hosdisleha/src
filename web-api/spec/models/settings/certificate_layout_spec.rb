# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe Settings::CertificateLayout, type: :model do

  describe 'self.organisation_chart' do

    let(:certificate_layout) { described_class.first }

    it 'delegates to the first record' do
      # Given
      certificate_layout.update(organisation_chart: Faker::Lorem.paragraph)
      described_class.create!(organisation_chart: Faker::Lorem.paragraph)
      # When
      result = described_class.organisation_chart
      # Then
      expect(result).to eq certificate_layout.organisation_chart
    end

  end

end
