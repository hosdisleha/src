# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe LogJob, type: :job do

  let!(:now) { Time.zone.now.iso8601 }
  let!(:user) { create :user }

  describe "concerning flowing consultations" do

    it "populates logs properly" do
      # Given
      intervention_domain = create :intervention_domain, :stream
      flowing_consultation = create :flowing_consultation, intervention_domain: intervention_domain
      created_action = Log::Action.new("created")
      assigned_action = Log::Action.new("assigned")
      actions2log = [created_action, assigned_action].map(&:to_json)
      # When
      expect {
        LogJob.perform_now(flowing_consultation, user, now, actions2log)
      # Then
      }.to change { Log.count }.by(1)
    end
  end

  describe "concerning appointments" do

    it "populates logs properly" do
      # Given
      consultation_type = create :consultation_type
      appointment = create :appointment, consultation_type: consultation_type
      created_action = Log::Action.new("created")
      postponed_action = Log::Action.new({action: "postponed", source: "umj"})
      actions2log = [created_action, postponed_action].map(&:to_json)
      # When
      expect {
        LogJob.perform_now(appointment, user, now, actions2log)
      # Then
      }.to change { Log.count }.by(1)
    end
  end
end
