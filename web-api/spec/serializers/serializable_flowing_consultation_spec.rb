# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe SerializableFlowingConsultation do

  let(:officer) { create :officer, id: 1, name: "Commissaire Swan Laurence", phone: "0607080900", email: "slaurence@filae.tld"}
  let(:patient) { create :patient, id: 2, first_name: "Denny", last_name: "Duquette", birth_year: 1966, gender: "unknown" }
  let(:intervention_domain) { create :intervention_domain, id: 1}
  let(:consultation_type) { create :consultation_type, id: 11, intervention_domain: intervention_domain}
  let(:calling_institution) { create :calling_institution, id: 21}
  let(:assignee) { create :user, id: 31}

  let(:consultation) {
    create :flowing_consultation, id: 42,
      called_at: "2018-05-21T21:42Z",
      onml_id: "someid",
      comment: "Lorem ipsum dolor sit amet",
      assigned_at: "2018-05-21T22:22Z",
      validated_at: "2018-05-22T00:23Z",
      cancelled_at: "2018-05-22T01:35Z",
      cancelled_by: "custody-ended",
      updated_at: "2018-05-21T21:42:42Z",
      officer: officer,
      patient: patient,
      intervention_domain: intervention_domain,
      consultation_type: consultation_type,
      calling_institution: calling_institution,
      assignee: assignee,
      concerns_minor: true
  }

  let(:inferrer) do
    Hash.new { |h, k| h[k] = Object.const_get("Serializable#{k}") }
  end

  it "complete flowing consultation returns all the fields" do
    # Given
    expected = {
      "type": :flowing_consultations,
      "id": "42",
      "attributes": {
        "called_at": "2018-05-21T21:42Z",
        "onml_id": "someid",
        "officer_name": "Commissaire Swan Laurence",
        "officer_phone": "0607080900",
        "officer_email": "slaurence@filae.tld",
        "officer_lambda_user": false,
        "comment": "Lorem ipsum dolor sit amet",
        "concerns_minor": true,
        "assigned_at": "2018-05-21T22:22Z",
        "validated_at": "2018-05-22T00:23Z",
        "cancelled_at": "2018-05-22T01:35Z",
        "cancelled_by": "custody-ended",
        "updated_at": "2018-05-21T21:42:42Z"
      },
      "relationships": {
        "intervention_domain": {
          "data": {
            "type": :intervention_domains,
            "id": "1"
          }
        },
        "consultation_type": {
          "data": {
            "type": :consultation_types,
            "id": "11"
          }
        },
        "calling_institution": {
          "data": {
            "type": :calling_institutions,
            "id": "21"
          }
        },
        "assignee": {
          "data": {
            "type": :users,
            "id": "31"
          }
        },
        "attached_files": {
          "data": []
        },
        "certificates": {
          "data": []
        },
        "patient": {
          "data": {
            "type": :patients,
            "id": "2"
          }
        }
      }
    }
    # When
    result = described_class.new(object: consultation, _class: inferrer).as_jsonapi
    # Then
    expect(result).to eq expected
  end

  it "not validated consultation does return validated_at field" do
    # Given
    consultation.validated_at = nil
    # When
    result = described_class.new(object: consultation, _class: inferrer).as_jsonapi
    # Then
    expect(result[:attributes]).to have_key(:validated_at)
  end

  it "not assigned consultation does return assigned_at field" do
    # Given
    consultation.assigned_at = nil
    # When
    result = described_class.new(object: consultation, _class: inferrer).as_jsonapi
    # Then
    expect(result[:attributes]).to have_key(:assigned_at)
  end

  it "not cancelled consultation does return cancelled_at field" do
    # Given
    consultation.cancelled_at = nil
    # When
    result = described_class.new(object: consultation, _class: inferrer).as_jsonapi
    # Then
    expect(result[:attributes]).to have_key(:cancelled_at)
  end

end
