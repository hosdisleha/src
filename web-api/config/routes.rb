# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
Rails.application.routes.draw do

  shallow do
    resources :intervention_domains do
      resources :consultation_types
      resources :schedules do
        resources :time_slots
      end
    end

    concern :attachable do
      resources :attached_files do
        get 'download', on: :member
      end
    end

    resources :appointments, except: :destroy do
      get 'certificate_preview', on: :member
      concerns :attachable
    end

    resources :calling_areas
    resources :calling_institutions

    resources :certificates, only: [:show, :update, :destroy] do
      get :preview, on: :member
      patch :send_by_mail, on: :member
      concerns :attachable
    end
    resources :certificate_templates, except: :destroy do
      resource :master_document, only: :create
    end
    resources :master_documents, only: %i[show destroy] do
      get 'download', on: :member
    end

    resources :consultation_reasons, except: %i[index]
    resources :consultation_subtypes, except: %i[index]
    resources :consultation_types, only: %i[index create]
    resources :duty_periods, only: %i[index show update]
    resources :flowing_consultations, except: :destroy do
      get 'last_updated', on: :collection
      concerns :attachable
      post 'certificates', on: :member
    end
    resources :medical_files, only: %i[show update]
    resources :patients, only: %i[index show update]
    resources :roles, only: %i[index show]
    resources :schedules, only: %i[create]
    resources :time_slots, only: %i[create]
    resources :users do
      concerns :attachable
    end

    namespace :settings do
      resource :certificate_layout, only: %i[show update]
    end
  end

  post 'password_requests', to: 'password_requests#create'

  use_doorkeeper
end
