# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationSubtypesController < ApplicationController
  before_action :set_consultation_subtype, only: [:show, :update, :destroy]
  deserializable_resource :consultation_subtype, only: [:create, :update]

  def show
    render jsonapi: @consultation_subtype
  end

  def create
    @consultation_subtype = ConsultationSubtype.new(permitted_attributes(ConsultationSubtype))
    authorize @consultation_subtype

    if @consultation_subtype.save
      render jsonapi: @consultation_subtype, status: :created, location: @consultation_subtype
    else
      render jsonapi_errors: @consultation_subtype.errors, status: :unprocessable_entity
    end
  end

  def update
    if @consultation_subtype.update(permitted_attributes(@consultation_subtype))
      render jsonapi: @consultation_subtype
    else
      render jsonapi_errors: @consultation_subtype.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @consultation_subtype.destroy
  end

  private

  def set_consultation_subtype
    @consultation_subtype = ConsultationSubtype.find(params[:id])
    authorize @consultation_subtype
  end
end
