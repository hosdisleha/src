# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class FlowingConsultationsController < ApplicationController
  before_action :set_flowing_consultation, only: [:show, :update, :certificates]
  deserializable_resource :flowing_consultation, only: [:create, :update]

  def index
    authorize FlowingConsultation
    intervention_domain = InterventionDomain.find(params[:filter][:intervention_domain_id])
    state = params[:filter][:state]
    sanitized_scope = policy_scope(FlowingConsultation)
    @flowing_consultations = sanitized_scope.of_intervention_domain(intervention_domain).sort_with_instructions(params[:sort])

    if state == "archive"
      unless ["administrator", "nurse", "physician", "secretary"].include?(current_user.role.try(:title))
        render status: :forbidden
      else
        @flowing_consultations = @flowing_consultations.
          archived.
          with_status(filter_param(:status)).
          between(filter_param(:action), filter_param(:start_date_time), filter_param(:end_date_time))

        if type = ConsultationType.find_by_id(filter_param(:consultation_type_id))
          @flowing_consultations = @flowing_consultations.of_consultation_type(type)
        end

        if calling_institution = CallingInstitution.find_by_id(filter_param(:calling_institution_id))
          @flowing_consultations = @flowing_consultations.of_calling_institution(calling_institution)
        end

        if patient_gender = filter_param(:patient_gender)
          @flowing_consultations = @flowing_consultations.of_patient_gender(patient_gender.presence)
        end

        if (patient_last_name = filter_param(:patient_last_name)).present?
          @flowing_consultations = @flowing_consultations.patient_last_name_like(patient_last_name)
        end

        if patient_age_range = filter_param(:patient_age_range)
          range_floor, range_ceiling = patient_age_range.split('-')
          @flowing_consultations = @flowing_consultations.of_patient_age_range(range_floor, range_ceiling) if range_floor.present? && range_ceiling.present?
        end

          if assignee = User.find_by_id(filter_param(:assignee_id))
            @flowing_consultations = @flowing_consultations.of_assignee(assignee)
          end
        render jsonapi: @flowing_consultations
      end
    elsif state == "active"
      @flowing_consultations = @flowing_consultations.active
      authorize @flowing_consultations

      render jsonapi: @flowing_consultations, include: %i[consultation_type patient calling_institution assignee]
    end
  end

  def show
    render jsonapi: @flowing_consultation
  end

  def last_updated
    intervention_domain = InterventionDomain.find(params[:filter][:intervention_domain_id])
    last_flowing_consultation = FlowingConsultation.of_intervention_domain(intervention_domain).last_updated

    authorize last_flowing_consultation
    render jsonapi: last_flowing_consultation, fields: {'flowing_consultations': [:updated_at] }
  end

  def create
    @flowing_consultation = FlowingConsultation.new(permitted_attributes(FlowingConsultation))
    @flowing_consultation.build_officer(officer_attributes)
    @flowing_consultation.build_patient(patient_attributes) unless params[:flowing_consultation][:patient_id].present?
    authorize @flowing_consultation

    if @flowing_consultation.save
      log_actions_for [Log::Action.new("created")], @flowing_consultation
      render jsonapi: @flowing_consultation, status: :created, location: @flowing_consultation
    else
      render jsonapi_errors: @flowing_consultation.errors, status: :unprocessable_entity
    end
  end

  def update
    @flowing_consultation.officer.assign_attributes(officer_attributes)
    @flowing_consultation.build_patient(patient_attributes) unless params[:flowing_consultation][:patient_id].present?
    @flowing_consultation.assign_attributes(permitted_attributes(@flowing_consultation))

    assignee_changes = @flowing_consultation.changes[:assignee_id]
    validation_changes = @flowing_consultation.changes[:validated_at]
    cancellation_changes = @flowing_consultation.changes[:cancelled_at]
    actions = []
    actions << Log::Action.new("#{action_prefix(assignee_changes)}assigned") if assignee_changes
    actions << Log::Action.new("#{action_prefix(validation_changes)}validated") if validation_changes
    if cancellation_changes
      action_name = "#{action_prefix(cancellation_changes)}cancelled"
      actions << if action_name == "cancelled"
        Log::Action.new({ action: action_name, source: @flowing_consultation.cancelled_by })
      else
        Log::Action.new(action_name)
      end
    end

    if @flowing_consultation.save
      log_actions_for(actions, @flowing_consultation)
      render jsonapi: @flowing_consultation
    else
      render jsonapi_errors: @flowing_consultation.errors, status: :unprocessable_entity
    end
  end

  def certificates
    attached_file = AttachedFile::SignedCertificate.new
    sanitize_filename
    attached_file.file.attach params[:file]
    certificate = Certificate.new(attached_files: [attached_file], certificatable: @flowing_consultation)
    if certificate.save
      render jsonapi: certificate
    else
      render jsonapi_errors: certificate.errors, status: :unprocessable_entity
    end
  end

  private

  def set_flowing_consultation
    @flowing_consultation = FlowingConsultation.find(params[:id])
    authorize @flowing_consultation
  end

  def officer_attributes
    {}.tap do |attrs|
      %w[officer_name officer_phone officer_email officer_lambda_user].each do |json_field|
        field = json_field.gsub(/officer_/,'')
        attrs[field] = params[:flowing_consultation][json_field]
      end
    end
  end

  def patient_attributes
    {}.tap do |attrs|
      %w[patient_first_name patient_last_name patient_birth_year patient_gender].each do |json_field|
        field = json_field.gsub(/patient_/,'')
        attrs[field] = params[:flowing_consultation][json_field]
      end
    end
  end

  def filter_param(sym)
    params[:filter][sym]
  end

  def sanitize_filename
    extension = params[:file].original_filename.split('.').last
    params[:file].original_filename = @flowing_consultation.signed_filename(extension)
  end
end
