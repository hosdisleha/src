# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class PasswordRequestsController < ApplicationController
  skip_before_action :doorkeeper_authorize!, only: [:create]
  skip_after_action :verify_authorized, only: [:create]
  deserializable_resource :password_request, only: [:create]

  def create
    identifier = params[:password_request][:identification]
    password_request = PasswordRequest.new(identifier)

    if password_request.valid?
      if user = User.find_by_identifier(identifier)
        user.password = SecureRandom.hex(10)
        if user.save!
          begin
            PasswordMailer.reset(user).deliver
          rescue => e
            logger.error "***** ERROR: #{e}"
            render status: :bad_gateway
          end
        else
          render status: :no_content
        end
      else
        render status: :not_found
      end
    else
      render jsonapi_errors: password_request.errors, status: :unprocessable_entity
    end
  end
end
