# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationTypesController < ApplicationController
  before_action :set_consultation_type, only: [:show, :update, :destroy]
  before_action :set_intervention_domain, only: [:index, :create]
  deserializable_resource :consultation_type, only: [:create, :update]

  def index
    @consultation_types = policy_scope(ConsultationType).all
    authorize @consultation_types

    render jsonapi: @consultation_types
  end

  def show
    render jsonapi: @consultation_type
  end

  def create
    sanitize_roles
    @consultation_type = if @intervention_domain
      @intervention_domain.consultation_types.build(permitted_attributes(ConsultationType))
    else
      ConsultationType.new(permitted_attributes(ConsultationType))
    end
    authorize @consultation_type

    if @consultation_type.save
      render jsonapi: @consultation_type, status: :created, location: @consultation_type
    else
      render jsonapi_errors: @consultation_type.errors, status: :unprocessable_entity
    end
  end

  def update
    sanitize_roles
    if @consultation_type.update(permitted_attributes(@consultation_type))
      render jsonapi: @consultation_type
    else
      render jsonapi_errors: @consultation_type.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @consultation_type.destroy
  end

  private

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find_by(id: params[:intervention_domain_id])
  end

  def set_consultation_type
    @consultation_type = ConsultationType.find(params[:id])
    authorize @consultation_type
  end

  def sanitize_roles
    sanitize_roles_for(params[:consultation_type][:role_ids])
    sanitize_roles_for(params[:consultation_type][:assigned_role_ids])
  end
end
