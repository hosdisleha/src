# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificatesController < ApplicationController
  before_action :set_certificate, only: [:show, :update, :preview, :destroy, :send_by_mail]
  deserializable_resource :certificate, only: [:update, :send_by_mail]

  def show
    render jsonapi: @certificate
  end

  def update
    @certificate.current_user = current_user
    @certificate.assign_attributes(permitted_attributes(@certificate))
    
    actions = {
      "validated" => @certificate.changes[:validated_at]
    }.map do |action_name, any_changes|
      Log::Action.new(action_prefix(any_changes) + action_name) if any_changes
    end.compact

    if @certificate.save
      log_actions_for actions, @certificate
      render jsonapi: @certificate
    else
      render jsonapi_errors: @certificate.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @certificate.destroy
  end

  def preview
    file = @certificate.signed_certificate.file
    if pdf_preview = PdfGenerator.new(file).generate
      send_data pdf_preview.data, filename: file.filename.to_s, type: pdf_preview.content_type
    else
      render status: 500
    end
  end

  def send_by_mail
    if (recipient = params[:certificate][:recipient]) && recipient.match?(/.*@.*\..*/)
      CertificateMailer.send_to_officer(recipient, @certificate).deliver_later
    else
      render status: 422
    end
  end

  private

  def set_certificate
    @certificate = Certificate.find(params[:id])
    authorize @certificate
  end
end
