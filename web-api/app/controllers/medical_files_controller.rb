# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MedicalFilesController < ApplicationController

  before_action :set_medical_file, only: [:show, :update]
  deserializable_resource :medical_file, only: [:update]

  def show
    render jsonapi: @medical_file
  end

  def update
    if @medical_file.update(medical_file_params)
      render jsonapi: @medical_file
    else
      render jsonapi: @medical_file.errors, status: :unprocessable_entity
    end
  end

  private

  def set_medical_file
    @medical_file = MedicalFile.find(params[:id])
    authorize @medical_file
  end

  def medical_file_params
    permitted_attributes(@medical_file)
  end

end
