# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CallingInstitutionsController < ApplicationController
  before_action :set_calling_institution, only: [:show, :update, :destroy]
  deserializable_resource :calling_institution, only: [:create, :update]

  def index
    @calling_institutions = policy_scope(CallingInstitution).all
    authorize @calling_institutions

    render jsonapi: @calling_institutions, include: params[:include]
  end

  def show
    render jsonapi: @calling_institution
  end

  def create
    @calling_institution = CallingInstitution.new(permitted_attributes(CallingInstitution))
    authorize @calling_institution

    if @calling_institution.save
      render jsonapi: @calling_institution, status: :created, location: @calling_institution
    else
      render jsonapi_errors: @calling_institution.errors, status: :unprocessable_entity
    end
  end

  def update
    if @calling_institution.update(permitted_attributes(@calling_institution))
      render jsonapi: @calling_institution
    else
      render jsonapi_errors: @calling_institution.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @calling_institution.destroy
  end

  private

  def set_calling_institution
    @calling_institution = CallingInstitution.find(params[:id])
    authorize @calling_institution
  end

end
