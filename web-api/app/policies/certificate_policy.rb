# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificatePolicy < ApplicationPolicy

  def show?
    connected_user?
  end

  def update?
    permitted_attributes.any?
  end

  def preview?
    can_manage? || can_access_to_listing?
  end

  def send_by_mail?
    acts_as_secretary?
  end

  def destroy?
    can_manage?
  end

  def permitted_attributes
    [].tap do |attrs|
      attrs.push(:validated_at, :validator_id, :delivered_at) if acts_as_secretary?
      attrs.push(:validated_at, :validator_id, :questionnaire, :data, questionnaire: {}, data: {}) if can_manage?
      attrs.uniq!
    end
  end

  private

  def acts_as_secretary?
    whitelist_roles([
      "nurse",
      "secretary"
    ]) || can_manage?
  end

  def can_manage?
    whitelist_roles([
      "administrator"
    ]) || record&.certificatable&.user_in_charge == user
  end

  def can_access_to_listing?
    record&.certificatable&.access_allowed_for_user?(user)
  end
end
