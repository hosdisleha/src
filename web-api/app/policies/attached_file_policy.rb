# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AttachedFilePolicy < ApplicationPolicy

  def show?
    is_adm_staff? || can_access_to_listing?
  end

  def download?
    is_adm_staff? || (can_access_to_listing? && medical_content_access?)
  end

  def create?
    is_adm_staff? || (can_access_to_listing? && can_take_charge?)
  end

  def destroy?
    is_adm_staff? || (can_access_to_listing? && owns_attachable?)
  end

  class Scope < Scope
    def resolve
      medical_content_access? ? scope : scope.none
    end
  end

  private

  def is_adm_staff?
    whitelist_roles([
      "administrator",
      "nurse",
      "secretary"
    ])
  end

  def can_take_charge?
    record.attachable.allowed_for_user?(user)
  end

  def owns_attachable?
    record.attachable.user_in_charge == user
  end

  def can_access_to_listing?
    record.attachable.access_allowed_for_user?(user)
  end
end
