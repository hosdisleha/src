# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AppointmentPolicy < ApplicationPolicy

  def index?
    connected_user?
  end

  def show?
    connected_user?
  end

  def create?
    can_manage?
  end

  def update?
    permitted_attributes.any?
  end

  def certificate_preview?
    can_manage?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end

  def permitted_attributes
    assigned_roles = record.is_a?(Class) ? [] : record.consultation_type.assigned_roles
    attributes = []
    if connected_user?
      if assigned_roles.include?(user.role)
        attributes += %i[
          id
          user_in_charge_id
        ]
      end
      if can_manage?
        attributes += %i[
          calling_institution_id
          cancelled_at
          cancelled_by
          circumstances
          comment
          consultation_reason_id
          consultation_subtype_id
          consultation_type_id
          duration
          emergency
          id
          official_report
          originating_appointment_id
          patient_arrived_at
          patient_id
          patient_missing
          patient_phone
          patient_released_at
          previous_appointment_id
          requisition_received_at
          starts_at
        ]
      end
    end
    attributes.uniq
  end

  private

  def can_manage?
    whitelist_roles([
      "administrator",
      "association",
      "nurse",
      "physician",
      "psychiatrist",
      "psychologist",
      "psychologist_expert",
      "secretary"
    ])
  end
end
