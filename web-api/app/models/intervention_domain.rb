# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class InterventionDomain < ApplicationRecord
  has_many :consultation_types, dependent: :destroy
  has_many :schedules, dependent: :destroy
  has_many :domain_authorizations, dependent: :destroy
  has_many :roles, through: :domain_authorizations

  enum modus_operandi: {
    appointments: 0,
    stream: 1
  }

  validates :title, presence: true, uniqueness: true
  validates :modus_operandi, presence: true
end
