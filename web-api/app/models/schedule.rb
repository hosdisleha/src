# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Schedule < ApplicationRecord
  belongs_to :intervention_domain
  has_many :time_slots, dependent: :destroy

  validates :activated_on, presence: true

  scope :on, ->(date) { where('activated_on <= ?', date).order('activated_on DESC').limit(1) }
  scope :for_consultation_type, ->(type) { type.intervention_domain.schedules }

  def self.duty_periods_for(consultation_type, date)
    schedule = self.for_consultation_type(consultation_type).on(date).first
    return [] unless schedule

    schedule.time_slots.where(consultation_type: consultation_type, week_day: date.cwday).map do |time_slot|
      DutyPeriod.find_or_create_by(time_slot: time_slot, scheduled_on: date)
    end
  end
end
