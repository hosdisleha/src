# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SerializableAppointment < SerializableBase
  type 'appointments'

  attributes :onml_id, :circumstances, :comment, :official_report, :cancelled_by, :emergency, :patient_missing, :patient_phone

  %w[starts_at cancelled_at patient_arrived_at patient_released_at].each do |field|
    attribute field do
      iso8601_datehourminute @object.send field
    end
  end

  attribute :updated_at do
    iso8601_datehourminutesec @object.updated_at.utc
  end

  attribute :created_at do
    iso8601_datehourminutesec @object.created_at.utc
  end

  attribute :duration do
    @object.duration.iso8601
  end

  %w[name phone email].each do |field|
    attribute "officer_#{field}" do
      @object.officer.send field
    end
  end

  attribute :requisition_received_at do
    iso8601_datehourminute @object.requisition_received_at
  end

  belongs_to :consultation_type do
    linkage always: true
  end

  belongs_to :consultation_subtype do
    linkage always: true
  end

  belongs_to :consultation_reason do
    linkage always: true
  end

  belongs_to :calling_institution do
    linkage always: true
  end

  belongs_to :user_in_charge do
    linkage always: true
  end

  belongs_to :certificate do
    linkage always: true
  end

  belongs_to :previous_appointment do
    linkage always: true
  end

  belongs_to :next_appointment do
    linkage always: true
  end

  has_many :attached_files do
    linkage always: true
  end

  has_many :complementary_appointments do
    linkage always: true
  end

  belongs_to :originating_appointment do
    linkage always: true
  end

  belongs_to :patient do
    linkage always: true
  end

  has_many :logs do
    data do
      Log.for_item(@object)
    end

    linkage always: true
  end

  has_one :medical_file do
    data do
      @object.actual_medical_file
    end

    linkage always: true
  end
end
