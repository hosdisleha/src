# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SerializableUser < SerializableBase
  type 'users'

  attributes :first_name, :last_name, :identifier, :email

  attribute :disabled_at do
    iso8601_datehourminute @object.disabled_at
  end

  belongs_to :role do
    linkage always: true
  end

  has_many :attached_files do
    data do
      ApplicationPolicy::Scope.new(@user, @object.attached_files).resolve
    end

    linkage always: true
  end
end
