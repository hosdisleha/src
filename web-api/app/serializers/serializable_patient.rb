# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SerializablePatient < SerializableBase
  extend JSONAPI::Serializable::Resource::ConditionalFields

  type 'patients'

  attributes :first_name, :last_name, :gender, :phone_numbers

  attribute :birthdate, unless: -> { @object.birthdate.nil? }
  attribute :birth_year, unless: -> { @object.birth_year.nil? }

  belongs_to :probably_duplicated_patient do
    linkage always: true
  end

  has_many :logs do
    data do
      Log.for_item(@object)
    end

    linkage always: true
  end

  has_many :appointments do
    linkage always: true
  end

  has_many :flowing_consultations do
    linkage always: true
  end
end
