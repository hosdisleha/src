# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SerializableCertificate < SerializableBase
  type 'certificates'

  attributes :questionnaire, :data, :checksum

  attribute :validated_at do
    iso8601_datehourminute @object.validated_at
  end

  belongs_to :validated_by do
    data do
      @object.validator
    end

    linkage always: true
  end

  attribute :delivered_at do
    iso8601_datehourminute @object.delivered_at
  end

  belongs_to :flowing_consultation do
    data do
      @object.certificatable if @object.certificatable.is_a?(FlowingConsultation)
    end

    linkage always: true
  end

  belongs_to :appointment do
    data do
      @object.certificatable if @object.certificatable.is_a?(Appointment)
    end

    linkage always: true
  end

  has_many :attached_files do
    linkage always: true
  end

end
