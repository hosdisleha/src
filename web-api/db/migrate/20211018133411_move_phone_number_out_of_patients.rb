# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MovePhoneNumberOutOfPatients < ActiveRecord::Migration[5.2]
  def change
    add_column(
      :appointments,
      :patient_phone,
      :string
    )
    add_column(
      :flowing_consultations,
      :patient_phone,
      :string
    )
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE appointments a
          SET patient_phone = p.phone
          FROM patients p
          WHERE a.patient_id = p.id;
        SQL
        execute <<-SQL
          UPDATE flowing_consultations f
          SET patient_phone = p.phone
          FROM patients p
          WHERE f.patient_id = p.id;
        SQL
      end
      dir.down do
      end
    end

    rename_column(
      :patients,
      :phone,
      :phone_deprecated_on_20211018
    )
  end
end
