# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
.DEFAULT_GOAL := test

# Life cycle
.PHONY: build up up-d stop ps restart restart-api restart-gui
build-api:
	docker-compose build --pull --build-arg USER_NAME=`id -nu` --build-arg USER_ID=`id -u` --build-arg GROUP_ID=`id -g` web-api

build-gui:
	docker-compose build --pull --build-arg USER_NAME=`id -nu` --build-arg USER_ID=`id -u` --build-arg GROUP_ID=`id -g` web-gui

build: build-api build-gui

up:
	docker-compose up

up-d:
	docker-compose up -d

stop:
	docker-compose stop

down:
	docker-compose down

ps:
	docker-compose ps

restart: restart-api restart-gui

restart-api:
	docker-compose restart web-api jobs

restart-gui:
	docker-compose restart web-gui

# Access
connect-api:
	docker-compose exec web-api bash -c 'cd web-api && bash'

connect-gui:
	docker-compose exec web-gui bash -c 'cd web-gui && bash'

# Tests
.PHONY: test-api test-api-rspec test-api-pacts test-gui test-gui-s test-uat test
test-api-rspec:
	docker-compose exec web-api bash -c 'cd web-api && bundle exec rake spec'

test-api-pacts:
	docker-compose exec web-api bash -c 'cd web-api && RAILS_ENV=test bundle exec rake pact:verify'

test-api: test-api-rspec test-api-pacts

test-gui:
	docker-compose exec web-gui bash -c 'cd web-gui && ember test'

test-gui-s:
	docker-compose exec web-gui bash -c 'cd web-gui && ember test -s'

test-uat:
	cd $(CURDIR)/web-uat && xvfb-run ./gwen features -b

test: up-d ps test-api test-gui test-uat stop

# Audit
.PHONY: audit-api
audit-api:
	docker-compose exec web-api bash -c 'cd web-api && bundle exec bundler-audit --update'

# License
.PHONY: license-fix license-check
license-fix:
	docker run -it --rm -v $(CURDIR):/github/workspace registry.infopiiaf.fr/license-eye:latest header fix

license-check:
	docker run -it --rm -v $(CURDIR):/github/workspace registry.infopiiaf.fr/license-eye:latest header check
