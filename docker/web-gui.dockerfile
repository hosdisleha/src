# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FROM node:14.21.3-buster-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends \
    firefox-esr \
    xvfb xauth \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends \
    sudo \
    git \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

RUN npm update -g yarn
RUN yarn global add ember-cli-update

RUN mkdir -p /app

ARG USER_NAME=root
ARG USER_ID=0
ARG GROUP_ID=0

RUN if [ "${USER_NAME}" != "root" ] ; then groupadd -o -g ${GROUP_ID} ${USER_NAME} \
  && useradd -o -l -u ${USER_ID} -g ${USER_NAME} -d /app/web-gui ${USER_NAME} \
  && usermod -aG sudo ${USER_NAME} \
  && echo "${USER_NAME} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/${USER_NAME} \
  && chown ${USER_NAME}:${USER_NAME} /app ; fi

USER ${USER_NAME}

WORKDIR /app
