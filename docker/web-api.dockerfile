# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FROM ruby:3.1.3-slim-buster

ARG DEBIAN_FRONTEND=noninteractive

# Base for Rails
RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends \
    build-essential \
    gnupg2 \
    curl \
    less \
    git \
    vim \
    xvfb xauth \
    nodejs \
    tmux \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

# Hack for postgresql-client-9.6 see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199
RUN mkdir -p /usr/share/man/man1 \
  && mkdir -p /usr/share/man/man7

# DB
RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends wget \
  && echo "deb http://apt.postgresql.org/pub/repos/apt buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
  && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && apt-get update -qq \
  && apt-get install -yq --no-install-recommends postgresql-server-dev-13 postgresql-client-13 \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends \
    fontconfig libjpeg62-turbo libxrender1 xfonts-75dpi xfonts-base \
    libreoffice-writer \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

RUN curl -LO https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb \
  && dpkg -i wkhtmltox_0.12.6-1.buster_amd64.deb

RUN gem update --system && \
    rm -rf $GEM_HOME/cache/*

RUN apt-get update -qq \
  && apt-get install -yq --no-install-recommends \
    sudo \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

RUN mkdir -p /app

ENV LANG=C.UTF-8 \
  BUNDLE_JOBS=4 \
  BUNDLE_RETRY=3 \
  BUNDLE_PATH=/app/vendor/bundle \
  TZ='/usr/share/zoneinfo/Europe/Paris'

ARG USER_NAME=root
ARG USER_ID=0
ARG GROUP_ID=0

RUN if [ "${USER_NAME}" != "root" ] ; then groupadd -o -g ${GROUP_ID} ${USER_NAME} \
  && useradd -o -l -u ${USER_ID} -g ${USER_NAME} -d /app/web-api ${USER_NAME} \
  && usermod -aG sudo ${USER_NAME} \
  && echo "${USER_NAME} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/${USER_NAME} \
  && chown ${USER_NAME}:${USER_NAME} /app ; fi

USER ${USER_NAME}

WORKDIR /app
