# Évolution discutée

Statut: Brouillon :pencil:

## Résumé

Quoi ?

## Motivation

Pourquoi ?

## Détails

Détailler autant que faire se peut, avec les avantages et inconvénients de chaque idée.

## Alternatives

S'il y en a...

## Questions non résolues

S'il y en a...
